/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <ntstatus.h>
#include <processenv.h>
#include <winbase.h>
#include <winnt.h>

#include "ntdll/nt.h"

static void write_binary(uintptr_t xx);
static void write_octal(uintptr_t xx);
static void write_hex(uintptr_t xx);

int main(void)
{
	puts("goo!\n");

	SetLastError(20U);
	if (20U == GetLastError()) {
		puts("SetLastError works!\n");
	}

	__wum_kernel_HANDLE input = GetStdHandle(STD_INPUT_HANDLE);
	__wum_kernel_HANDLE input_dup;
	if (NtDuplicateObject(GetCurrentProcess(), input,
	                      GetCurrentProcess(), &input_dup, 0, 0,
	                      DUPLICATE_SAME_ACCESS) != 0) {
		perror("NtDuplicateObject");
		return EXIT_FAILURE;
	}

	if (NtClose(input) != 0) {
		perror("NtClose");
		return EXIT_FAILURE;
	}

	puts("starting!\n");
	puts("foo!\n");

	void *addr = 0U;
	size_t size = 4096U;
	if (NtAllocateVirtualMemory(GetCurrentProcess(), &addr, 0U,
	                            &size, MEM_COMMIT,
	                            PAGE_READWRITE) != 0) {
		perror("NtAllocateVirtualMemory");
		return EXIT_FAILURE;
	}
	puts("bar!\n");
	write_hex((uintptr_t)addr);

	{
		size_t ii;
		for (ii = 0U; ii < 4096U; ++ii)
			((char *)addr)[ii] = (char)0xF0U;
	}

	if (NtFreeVirtualMemory(GetCurrentProcess(), &addr, &size,
	                        MEM_RELEASE) != 0) {
		perror("NtFreeVirtualMemory");
		return EXIT_FAILURE;
	}

	puts("hello\n");
	*((char volatile *)0xdeadbeef) = 43;
	puts("hello\n");

	return EXIT_SUCCESS;
}

static void write_binary(uintptr_t xx)
{
	write(STDOUT_FILENO, "0b", sizeof "0b" - 1U);
	unsigned char ii;
	for (ii = 64U; ii > 0U;) {
		--ii;
		uintptr_t bit;
		if (0 == ii) {
			bit = UINTPTR_C(1);
		} else {
			bit = UINTPTR_C(1) << (uintptr_t)ii;
		}
		if ((xx & bit) != 0) {
			write(STDOUT_FILENO, "1", sizeof "1" - 1U);
		} else {
			write(STDOUT_FILENO, "0", sizeof "0" - 1U);
		}
	}
	write(STDOUT_FILENO, "\n", sizeof "\n" - 1U);
}

static void write_octal(uintptr_t xx)
{
	write(STDOUT_FILENO, "0", sizeof "0" - 1U);
	unsigned char ii;
	for (ii = (64U / 8U); ii > 0U;) {
		--ii;
		uintptr_t bit;
		if (0 == ii) {
			bit = UINTPTR_C(1);
		} else {
			bit = UINTPTR_C(1) << 3U * (uintptr_t)ii;
		}
		uintptr_t n =
		    '0' + ((xx & bit) >> (3U * (uintptr_t)(ii - 1U)));
		write(STDOUT_FILENO, &n, 1U);
	}
	write(STDOUT_FILENO, "\n", sizeof "\n" - 1U);
}

static void write_hex(uintptr_t xx)
{
	write(STDOUT_FILENO, "0x", sizeof "0x" - 1U);

	uintptr_t yy = 1U;
	size_t ii;
	for (ii = 0U; ii < 11U; ++ii)
		yy *= 16U;

	for (;;) {
		char strs[] = {
		    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',

		    'A', 'B', 'C', 'D', 'E', 'F',
		};

		write(STDOUT_FILENO, &strs[(xx / yy) % 16U], 1U);
		yy /= 16U;
		if (0U == yy) {
			break;
		}
	}
	write(STDOUT_FILENO, "\n", sizeof "\n" - 1U);
}
