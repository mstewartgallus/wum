/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef __WUM_KERNEL_ABI_H
#define __WUM_KERNEL_ABI_H 1

#include "wum-kernel/stdint.h"
#include "wum-kernel/winternl.h"

struct _wum_kernel_abi_read_write_page_value {
	char __padding1[2304U];

	struct _wum_kernel_PEB peb;

	struct _wum_kernel_RTL_USER_PROCESS_PARAMETERS
	    process_parameters;

	__wum_kernel_uint32_t wakeup_kernel_thread;
	__wum_kernel_uint32_t __padding2;

	__wum_kernel_uint32_t have_pending_system_call;

	__wum_kernel_uint64_t system_call;
	__wum_kernel_uint64_t arguments[9U];

	__wum_kernel_uint32_t syscall_completed;
	__wum_kernel_uint32_t __padding3;
};

union _wum_kernel_abi_read_write_page {
	struct _wum_kernel_abi_read_write_page_value value;
	char __padding3[4096U];
};

union _wum_kernel_abi_read_only_page {
	struct {
		__wum_kernel_uint64_t error;
	} value;

	char __padding3[4096U];
};

union _wum_kernel_abi_execute_only_page {
	struct {
		char bytes[4096U];
	} value;

	char __padding3[4096U];
};

struct _wum_kernel_abi_shared_pages {
	union _wum_kernel_abi_read_write_page read_write;
	union _wum_kernel_abi_read_only_page read_only;
	union _wum_kernel_abi_execute_only_page execute_only;
};

#define _WUM_KERNEL_ABI_SHARED_PAGES 2012995584UL

#define _WUM_KERNEL_ABI_SHARED_PAGESP                                  \
	((struct                                                       \
	  _wum_kernel_abi_shared_pages volatile *)(_WUM_KERNEL_ABI_SHARED_PAGES))

#define _WUM_KERNEL_ABI_SHARED_PAGES_SIZE                              \
	(sizeof(struct _wum_kernel_abi_shared_pages))

#define _WUM_KERNEL_ABI_SYSCALL_NT_WRITE_FILE _WUM_KERNEL_UINT64_C(1)
#define _WUM_KERNEL_ABI_SYSCALL_NT_TERMINATE_PROCESS                   \
	_WUM_KERNEL_UINT64_C(2)
#define _WUM_KERNEL_ABI_SYSCALL_NT_ALLOCATE_VIRTUAL_MEMORY             \
	_WUM_KERNEL_UINT64_C(3)
#define _WUM_KERNEL_ABI_SYSCALL_NT_FREE_VIRTUAL_MEMORY                 \
	_WUM_KERNEL_UINT64_C(4)
#define _WUM_KERNEL_ABI_SYSCALL_NT_CLOSE _WUM_KERNEL_UINT64_C(5)

#define _WUM_KERNEL_ABI_SYSCALL_NT_DUPLICATE_OBJECT                    \
	_WUM_KERNEL_UINT64_C(6)

#endif
