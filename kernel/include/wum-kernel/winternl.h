/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_KERNEL_WINTERNL_H
#define _WUM_KERNEL_WINTERNL_H 1

#include "wum-kernel/wchar.h"
#include "wum-kernel/winbase.h"
#include "wum-kernel/stdint.h"

struct _wum_kernel_RTL_USER_PROCESS_PARAMETERS {
	__wum_kernel_HANDLE hStdInput;
	__wum_kernel_HANDLE hStdOutput;
	__wum_kernel_HANDLE hStdError;
};

struct __wum_kernel_LIST_ENTRY {
	struct __wum_kernel_LIST_ENTRY *Flink;
	struct __wum_kernel_LIST_ENTRY *Blink;
};

struct _wum_kernel_PEB_LDR_DATA {
	char Reserved1[8U];
	void *Reserved2[3U];
	struct __wum_kernel_LIST_ENTRY InMemoryOrderModuleList;
};

struct _wum_kernel_PEB {
	char Reserved1[2U];
	char BeingDebugged;
	char Reserved2[21U];
	struct _wum_kernel_PEB_LDR_DATA LoaderData;
	struct _wum_kernel_RTL_USER_PROCESS_PARAMETERS *
	    ProcessParameters;
	char Reserved3[520U];
	void (*PostProcessInitRoutine)(void);
	char Reserved4[136U];
	__wum_kernel_uint32_t SessionId;
};

struct _wum_kernel_NT_TIB {
	void *ExceptionList;
	void *StackBase;
	void *StackLimit;
	void *SubSystemTib;
	union {
		void *FiberData;
		__wum_kernel_uint64_t Version;
	} DUMMYUNIONNAME;
	void *ArbitraryUserPointer;
	struct _wum_kernel_NT_TIB *Self;
};

struct __wum_kernel_CLIENT_ID {
	__wum_kernel_HANDLE UniqueProcess;
	__wum_kernel_HANDLE UniqueThread;
};

struct __wum_kernel_GDI_TEB_BATCH {
	__wum_kernel_uint32_t Offset;
	__wum_kernel_HANDLE HDC;
	__wum_kernel_uint32_t Buffer[0x136U];
};

struct __wum_kernel_UNICODE_STRING {
	__wum_kernel_uint16_t Length;
	__wum_kernel_uint16_t MaximumLength;
	__wum_kernel_wchar_t *Buffer;
};

struct __wum_kernel_RTL_ACTIVATION_CONTEXT_STACK_FRAME;

struct __wum_kernel_ACTIVATION_CONTEXT_STACK {
	__wum_kernel_uint32_t Flags;
	__wum_kernel_uint32_t NextCookieSequenceNumber;
	struct __wum_kerneL_RTL_ACTIVATION_CONTEXT_STACK_FRAME *
	    ActiveFrame;
	struct __wum_kernel_LIST_ENTRY FrameListCache;
};

struct _wum_kernel_TEB {
	struct _wum_kernel_NT_TIB Tib;
	void *EnvironmentPointer;
	struct __wum_kernel_CLIENT_ID ClientId;
	void *ActiveRpcHandle;
	void *ThreadLocalStoragePointer;
	struct _wum_kernel_PEB *Peb;
	__wum_kernel_uint32_t LastErrorValue;
	__wum_kernel_uint32_t CountOfOwnedCriticalSections;
	void *CsrClientThread;
	void *Win32ThreadInfo;
	__wum_kernel_uint32_t Win32ClientInfo[31];
	void *WOW32Reserved;
	__wum_kernel_uint32_t CurrentLocale;
	__wum_kernel_uint32_t FpSoftwareStatusRegister;
	void *SystemReserved1[54];
	__wum_kernel_int32_t ExceptionCode;
	struct __wum_kernel_ACTIVATION_CONTEXT_STACK
	    ActivationContextStack;
	char SpareBytes1[24];
	void *SystemReserved2[10];
	struct __wum_kernel_GDI_TEB_BATCH GdiTebBatch;
	__wum_kernel_HANDLE gdiRgn;
	__wum_kernel_HANDLE gdiPen;
	__wum_kernel_HANDLE gdiBrush;
	struct __wum_kernel_CLIENT_ID RealClientId;
	__wum_kernel_HANDLE GdiCachedProcessHandle;
	__wum_kernel_uint32_t GdiClientPID;
	__wum_kernel_uint32_t GdiClientTID;
	void *GdiThreadLocaleInfo;
	__wum_kernel_uint32_t UserReserved[5];
	void *glDispachTable[280];
	void *glReserved1[26];
	void *glReserved2;
	void *glSectionInfo;
	void *glSection;
	void *glTable;
	void *glCurrentRC;
	void *glContext;
	__wum_kernel_uint32_t LastStatusValue;
	struct __wum_kernel_UNICODE_STRING StaticUnicodeString;
	__wum_kernel_wchar_t StaticUnicodeBuffer[261];
	void *DeallocationStack;
	void *TlsSlots[64];
	struct __wum_kernel_LIST_ENTRY TlsLinks;
	void *Vdm;
	void *ReservedForNtRpc;
	void *DbgSsReserved[2];
	__wum_kernel_uint32_t HardErrorDisabled;
	void *Instrumentation[16];
	void *WinSockData;
	__wum_kernel_uint32_t GdiBatchCount;
	__wum_kernel_uint32_t Spare2;
	void *Spare3;
	void *Spare4;
	void *ReservedForOle;
	__wum_kernel_uint32_t WaitingOnLoaderLock;
	void *Reserved5[3];
	void **TlsExpansionSlots;
	__wum_kernel_uint32_t ImpersonationLocale;
	__wum_kernel_uint32_t IsImpersonating;
	void *NlsCache;
	void *ShimData;
	__wum_kernel_uint32_t HeapVirtualAffinity;
	void *CurrentTransactionHandle;
	void *ActiveFrame;
};

#endif
