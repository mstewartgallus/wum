/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_KERNEL_FUTEX_H
#define _WUM_KERNEL_FUTEX_H 1

#include "wum-kernel/native_syscall.h"
#include "wum-kernel/stddef.h"

#define __NR_futex 202

#define FUTEX_WAIT 0
#define FUTEX_WAKE 1

struct _wum_kernel_timespec;

static inline int
_wum_kernel_futex_wait(void volatile *uaddr, int val,
                       struct _wum_kernel_timespec const *timeout);
static inline int _wum_kernel_futex_wake(unsigned *wokeupp,
                                         void volatile *uaddr, int val);

static inline int
_wum_kernel_futex_wait(void volatile *uaddr, int val,
                       struct _wum_kernel_timespec const *timeout)
{
	return _wum_kernel_native_syscall_4(
	    __NR_futex, 0, (__wum_kernel_intptr_t)uaddr, FUTEX_WAIT,
	    val, (__wum_kernel_intptr_t)timeout);
}

static inline int _wum_kernel_futex_wake(unsigned *wokeupp,
                                         void volatile *uaddr, int val)
{
	__wum_kernel_intptr_t xx;
	int errnum;

	if ((errnum = _wum_kernel_native_syscall_3(
	         __NR_futex, &xx, (__wum_kernel_intptr_t)uaddr,
	         FUTEX_WAKE, val)) != 0) {
		return errnum;
	}

	if (wokeupp != 0)
		*wokeupp = xx;

	return 0;
}

#if defined __has_builtin
#define WUM_HAS_BUILTIN(X) __has_builtin(X)
#else
#define WUM_HAS_BUILTIN(X) 0
#endif

#if WUM_HAS_BUILTIN(__builtin_ia32_pause)
#define WUM_PAUSE() __builtin_ia32_pause()
#else
#define WUM_PAUSE() __asm__ volatile("pause");
#endif

#endif
