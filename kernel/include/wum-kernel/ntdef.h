#ifndef _WUM_KERNEL_NTDEF_H
#define _WUM_KERNEL_NTDEF_H 1

#include "wum-kernel/stdint.h"

typedef __wum_kernel_uint32_t __wum_kernel_NTSTATUS;

#endif
