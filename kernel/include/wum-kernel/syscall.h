/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef __WUM_KERNEL_SYSCALL_H
#define __WUM_KERNEL_SYSCALL_H 1

#include "wum-kernel/abi.h"
#include "wum-kernel/futex.h"

#include "wum-kernel/stdint.h"

static inline __wum_kernel_uint64_t
_wum_kernel_syscall_0(__wum_kernel_uint64_t sysno);
static inline __wum_kernel_uint64_t
_wum_kernel_syscall_1(__wum_kernel_uint64_t sysno,
                      __wum_kernel_uint64_t arg1);
static inline __wum_kernel_uint64_t
_wum_kernel_syscall_2(__wum_kernel_uint64_t sysno,
                      __wum_kernel_uint64_t arg1,
                      __wum_kernel_uint64_t arg2);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_3(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_4(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_5(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_6(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_7(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_8(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7,
    __wum_kernel_uint64_t arg8);
static inline __wum_kernel_uint64_t _wum_kernel_syscall_9(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7,
    __wum_kernel_uint64_t arg8, __wum_kernel_uint64_t arg9);

static inline __wum_kernel_uint64_t
_wum_kernel_syscall_0(__wum_kernel_uint64_t sysno)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.system_call =
	    sysno;
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value
	    .syscall_completed = 0U;

	__atomic_thread_fence(__ATOMIC_RELEASE);

	__atomic_store_n(&_WUM_KERNEL_ABI_SHARED_PAGESP->read_write
	                      .value.have_pending_system_call,
	                 1U, __ATOMIC_RELEASE);

	__atomic_store_n(&_WUM_KERNEL_ABI_SHARED_PAGESP->read_write
	                      .value.wakeup_kernel_thread,
	                 1U, __ATOMIC_RELEASE);

	/* In the best case avoid a system call */
	if (__atomic_load_n(&_WUM_KERNEL_ABI_SHARED_PAGESP->read_write
	                         .value.wakeup_kernel_thread,
	                    __ATOMIC_ACQUIRE))
		_wum_kernel_futex_wake(
		    0, &_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value
		            .wakeup_kernel_thread,
		    1);

	for (;;) {
		unsigned char ii;
		for (ii = 0U; ii < 20U; ++ii) {
			__wum_kernel_uint32_t expected = 1U;
			if (__atomic_compare_exchange_n(
			        &_WUM_KERNEL_ABI_SHARED_PAGESP
			             ->read_write.value
			             .syscall_completed,
			        &expected, 0U, 1U, __ATOMIC_SEQ_CST,
			        __ATOMIC_SEQ_CST))
				goto read_completed;

			WUM_PAUSE();
		}

		_wum_kernel_futex_wait(
		    &_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value
		         .syscall_completed,
		    0, 0);
	}

read_completed:
	__atomic_thread_fence(__ATOMIC_ACQUIRE);

	__wum_kernel_uint64_t error =
	    _WUM_KERNEL_ABI_SHARED_PAGESP->read_only.value.error;

	return error;
}

static inline __wum_kernel_uint64_t
_wum_kernel_syscall_1(__wum_kernel_uint64_t sysno,
                      __wum_kernel_uint64_t arg1)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[0U] =
	    arg1;
	return _wum_kernel_syscall_0(sysno);
}

static inline __wum_kernel_uint64_t
_wum_kernel_syscall_2(__wum_kernel_uint64_t sysno,
                      __wum_kernel_uint64_t arg1,
                      __wum_kernel_uint64_t arg2)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[1U] =
	    arg2;
	return _wum_kernel_syscall_1(sysno, arg1);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_3(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[2U] =
	    arg3;
	return _wum_kernel_syscall_2(sysno, arg1, arg2);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_4(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[3U] =
	    arg4;
	return _wum_kernel_syscall_3(sysno, arg1, arg2, arg3);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_5(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[4U] =
	    arg5;
	return _wum_kernel_syscall_4(sysno, arg1, arg2, arg3, arg4);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_6(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[5U] =
	    arg6;
	return _wum_kernel_syscall_5(sysno, arg1, arg2, arg3, arg4,
	                             arg5);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_7(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[6U] =
	    arg7;
	return _wum_kernel_syscall_6(sysno, arg1, arg2, arg3, arg4,
	                             arg5, arg6);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_8(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7,
    __wum_kernel_uint64_t arg8)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[7U] =
	    arg8;
	return _wum_kernel_syscall_7(sysno, arg1, arg2, arg3, arg4,
	                             arg5, arg6, arg7);
}

static inline __wum_kernel_uint64_t _wum_kernel_syscall_9(
    __wum_kernel_uint64_t sysno, __wum_kernel_uint64_t arg1,
    __wum_kernel_uint64_t arg2, __wum_kernel_uint64_t arg3,
    __wum_kernel_uint64_t arg4, __wum_kernel_uint64_t arg5,
    __wum_kernel_uint64_t arg6, __wum_kernel_uint64_t arg7,
    __wum_kernel_uint64_t arg8, __wum_kernel_uint64_t arg9)
{
	_WUM_KERNEL_ABI_SHARED_PAGESP->read_write.value.arguments[8U] =
	    arg9;
	return _wum_kernel_syscall_8(sysno, arg1, arg2, arg3, arg4,
	                             arg5, arg6, arg7, arg8);
}

#endif
