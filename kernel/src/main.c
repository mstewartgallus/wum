/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _GNU_SOURCE

#include "config.h"

#include "wum-kernel/abi.h"
#include "wum-kernel/futex.h"
#include "wum-kernel/ntdef.h"
#include "wum-kernel/ntstatus.h"
#include "wum-kernel/winnt.h"

#include "_wum-kernel/closefrom.h"
#include "_wum-kernel/ptrace.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <poll.h>
#include <pthread.h>
#include <sched.h>
#include <signal.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syscall.h>
#include <sys/eventfd.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/signalfd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <ucontext.h>
#include <unistd.h>

#include <linux/ptrace.h>

#define ARRAY_LENGTH(...) (sizeof __VA_ARGS__ / sizeof(__VA_ARGS__)[0U])

typedef __wum_kernel_NTSTATUS NTSTATUS;
typedef struct _wum_kernel_IMAGE_DOS_HEADER IMAGE_DOS_HEADER;
typedef struct _wum_kernel_IMAGE_FILE_HEADER IMAGE_FILE_HEADER;
typedef struct _wum_kernel_IMAGE_DATA_DIRECTORY IMAGE_DATA_DIRECTORY;
typedef struct _wum_kernel_IMAGE_OPTIONAL_HEADER IMAGE_OPTIONAL_HEADER;
typedef struct _wum_kernel_IMAGE_SECTION_HEADER IMAGE_SECTION_HEADER;

#define IMAGE_FILE_HEADER_SIZE _WUM_KERNEL_IMAGE_FILE_HEADER_SIZE
#define IMAGE_OPTIONAL_HEADER_SIZE                                     \
	_WUM_KERNEL_IMAGE_OPTIONAL_HEADER_SIZE

#define IMAGE_SCN_CNT_CODE _WUM_KERNEL_IMAGE_SCN_CNT_CODE
#define IMAGE_SCN_CNT_INITIALIZED_DATA                                 \
	_WUM_KERNEL_IMAGE_SCN_CNT_INITIALIZED_DATA
#define IMAGE_SCN_CNT_UNINITIALIZED_DATA                               \
	_WUM_KERNEL_IMAGE_SCN_CNT_UNINITIALIZED_DATA
#define IMAGE_SCN_LNK_NRELOC_OVFL _WUM_KERNEL_IMAGE_SCN_LNK_NRELOC_OVFL
#define IMAGE_SCN_MEM_DISCARDABLE _WUM_KERNEL_IMAGE_SCN_MEM_DISCARDABLE
#define IMAGE_SCN_MEM_NOT_CACHED _WUM_KERNEL_IMAGE_SCN_MEM_NOT_CACHED
#define IMAGE_SCN_MEM_NOT_PAGED _WUM_KERNEL_IMAGE_SCN_MEM_NOT_PAGED
#define IMAGE_SCN_MEM_SHARED _WUM_KERNEL_IMAGE_SCN_MEM_SHARED
#define IMAGE_SCN_MEM_EXECUTE _WUM_KERNEL_IMAGE_SCN_MEM_EXECUTE
#define IMAGE_SCN_MEM_READ _WUM_KERNEL_IMAGE_SCN_MEM_READ
#define IMAGE_SCN_MEM_WRITE _WUM_KERNEL_IMAGE_SCN_MEM_WRITE

#define ADDRESS_SPACE_TOP (0x800000000000ULL - 4096U)

#define CURRENT_PROCESS_HANDLE ((uint64_t)-1)

extern char const _wum_kernel_syscall_instr_start;
extern char const _wum_kernel_syscall_instr_end;

struct handle;
struct process;
struct vma;

static NTSTATUS errno_to_ntstatus(int err);
static char const *ntstatus_str(NTSTATUS status);

static void push_exit_event(struct process *process,
                            uint32_t exit_value);
static void wakeup_process(struct process *process);
static int stop_process(struct process *process);

static void process_mark_vma_as_stale(struct process *process,
                                      struct vma *vma);
static void process_gc_stale_vmas(struct process *process);

static int process_memcpy(struct process *process, uint64_t dest,
                          void const *src, size_t size);

static int process_load_library(struct process *process,
                                char const *binary_name,
                                uint64_t *entry_pointp);

static void print_mmap_info(struct process *process);
static int process_mmap(struct process *process, uint64_t *resultp,
                        uint64_t addr, uint64_t length, int prot,
                        int flags, struct handle *handle,
                        uint64_t offset);
static int process_munmap(struct process *process, uint64_t addr,
                          uint64_t length);
static int process_mprotect(struct process *process, uint64_t addr,
                            size_t length, int prot);

static NTSTATUS create_anonymous_memory_handle(struct handle **handlep,
                                               char const *name);
static NTSTATUS create_fd_handle(struct handle **handlep, int fd,
                                 char const *name);
static struct handle *process_handle_get(struct process *process,
                                         uint64_t handle);
static struct handle *handle_get(struct handle *handle);
static void handle_put(struct handle *handle);

static int nt_allocate_virtual_memory(
    struct process *process, uint64_t process_handle,
    uint64_t base_addressp, uint64_t zero_bits, uint64_t region_sizep,
    uint64_t allocation_type, uint64_t protect);
static int nt_free_virtual_memory(struct process *process,
                                  uint64_t process_handle,
                                  uint64_t base_addressp,
                                  uint64_t region_sizep,
                                  uint64_t free_type);
static int nt_write_file(struct process *process, uint64_t hFile,
                         uint64_t hEvent, uint64_t apc,
                         uint64_t apc_user, uint64_t io_status,
                         uint64_t buffer, uint64_t length,
                         uint64_t offset, uint64_t key);

static int nt_close(struct process *process, uint64_t hFile);

static int
nt_duplicate_object(struct process *process,
                    uint64_t SourceProcessHandle, uint64_t SourceHandle,
                    uint64_t TargetProcessHandle, uint64_t TargetHandle,
                    uint64_t DesiredAccess, uint64_t HandleAttributes,
                    uint64_t Options);

static int proxy_mprotect(struct process *process, uint64_t addr,
                          size_t length, int prot);

static int proxy_brk(struct process *process, uint64_t addr);

static int proxy_write(struct process *process, uint64_t *resultp,
                       int fd, uint64_t buf, size_t count);

static int proxy_mmap(struct process *process, uint64_t *resultp,
                      uint64_t addr, size_t length, int prot, int flags,
                      int fd, off_t offset);
static int proxy_munmap(struct process *process, uint64_t addr,
                        size_t length);
static int proxy_pread(struct process *process, uint64_t *resultp,
                       int fd, uint64_t buf, size_t count,
                       off_t offset);
static uint64_t proxy_syscall(struct process *process, uint64_t sysno,
                              uint64_t *resultp, uint64_t arg1,
                              uint64_t arg2, uint64_t arg3,
                              uint64_t arg4, uint64_t arg5,
                              uint64_t arg6);

static bool check_and_clear_flag(uint32_t volatile *var);
static void set_and_wake_flag(uint32_t volatile *var);

static int child_func(void *arg);
static void *process_thread_routine(void *arg);

struct handle {
	unsigned refcount;
	int fd;
	char *name;
};

struct vma_node {
	struct vma *_vma;
	struct vma_node *_higher_node;
};

struct vma_index {
	struct vma_node *_node;
};
struct vma {
	struct handle *handle;
	uint64_t addr_bottom;
	uint64_t addr_top;
	uint64_t offset;
	bool readable : 1U;
	bool writable : 1U;
	bool executable : 1U;
	bool shared : 1U;
	bool stale : 1U;
};
struct vmas {
	struct vma_node *_lowest_node;
};

static struct vma_index get_lowest_vma(struct vmas const *vmas)
{
	return (struct vma_index){vmas->_lowest_node};
}
static bool index_is_valid(struct vmas const *vmas, struct vma_index ii)
{
	return ii._node != 0;
}
static struct vma *get_vma(struct vmas const *vmas, struct vma_index ii)
{
	assert(ii._node != 0);
	return ii._node->_vma;
}

static struct vma_index get_next_vma(struct vmas const *vmas,
                                     struct vma_index ii)
{
	return (struct vma_index){ii._node->_higher_node};
}

#define INVALID_VMA_INDEX ((struct vma_index){0})

#define NUM_HANDLES 2048U

struct process {
	uint32_t has_pending_child_signal;

	uint32_t have_pending_exit;
	uint32_t pending_exit_value;

	ucontext_t normal_context;
	ucontext_t syscall_context;

	bool syscall_context_not_set;

	int syscall_result;
	pid_t pid;
	int exit_fd;
	int mem_fd;
	char const *binary_name;
	struct _wum_kernel_abi_shared_pages *shared_pages;
	struct handle *anonymous_memory;
	struct vmas vmas;
	struct vmas stale_vmas;
	struct handle *handle_table[1024U];
};

#define LOWEST_HANDLE 4096U
#define UNUSED_BITS 2U

struct child_func_args {
	struct handle *anonymous_memory;
};

int main(int argc, char **argv)
{
	if (argc < 2) {
		return EXIT_FAILURE;
	}

	char const *binary_name = argv[1U];

	int err = 0;
	NTSTATUS status = 0;

	wum_kernel_closefrom(3);

	struct handle *anonymous_memory;
	{
		struct handle *xx;
		status = create_anonymous_memory_handle(&xx, 0);
		if (status != 0) {
			fprintf(stderr,
			        "create_anonymous_memory_handle: %s",
			        ntstatus_str(status));
			return EXIT_FAILURE;
		}
		anonymous_memory = xx;
	}

	struct handle *stdin_handle;
	{
		struct handle *xx;
		status =
		    create_fd_handle(&xx, STDIN_FILENO, "/dev/stdin");
		if (status != 0) {
			fprintf(stderr, "create_fd_handle: %s",
			        ntstatus_str(status));
			return EXIT_FAILURE;
		}
		stdin_handle = xx;
	}

	struct handle *stdout_handle;
	{
		struct handle *xx;
		status =
		    create_fd_handle(&xx, STDOUT_FILENO, "/dev/stdout");
		if (status != 0) {
			fprintf(stderr, "create_fd_handle: %s",
			        ntstatus_str(status));
			return EXIT_FAILURE;
			return EXIT_FAILURE;
		}
		stdout_handle = xx;
	}

	struct handle *stderr_handle;
	{
		struct handle *xx;
		status =
		    create_fd_handle(&xx, STDERR_FILENO, "/dev/stderr");
		if (status != 0) {
			fprintf(stderr, "create_fd_handle: %s",
			        ntstatus_str(status));
			return EXIT_FAILURE;
		}
		stderr_handle = xx;
	}

	struct _wum_kernel_abi_shared_pages *shared_pages =
	    mmap(0, _WUM_KERNEL_ABI_SHARED_PAGES_SIZE,
	         PROT_READ | PROT_WRITE, MAP_SHARED,
	         anonymous_memory->fd, _WUM_KERNEL_ABI_SHARED_PAGES);
	if (MAP_FAILED == shared_pages) {
		fprintf(stderr, "mmap: %s",
		        ntstatus_str(errno_to_ntstatus(errno)));
		return EXIT_FAILURE;
	}

	memcpy((void *)shared_pages->execute_only.value.bytes,
	       &_wum_kernel_syscall_instr_start,
	       &_wum_kernel_syscall_instr_end -
	           &_wum_kernel_syscall_instr_start);

	void *child_stack = mmap(0, 20U * 4096U, PROT_READ | PROT_WRITE,
	                         MAP_PRIVATE | MAP_STACK |
	                             MAP_ANONYMOUS | MAP_GROWSDOWN,
	                         -1, 0);
	if (MAP_FAILED == child_stack) {
		perror("mmap");
		return EXIT_FAILURE;
	}
	void *child_stack_end = ((char *)child_stack) + 20U * 4096U;

	pid_t child;
	{
		struct child_func_args args = {.anonymous_memory =
		                                   anonymous_memory};

		child = clone(child_func, child_stack_end,
		              SIGCHLD | CLONE_FILES | CLONE_UNTRACED |
		                  CLONE_FS,
		              &args);
		if (-1 == child) {
			fprintf(stderr, "clone: %s",
			        ntstatus_str(errno_to_ntstatus(errno)));
			return EXIT_FAILURE;
		}
		waitpid(child, 0, WSTOPPED);
	}

	int child_mem;
	{
		char mem_str[] = "/proc/XXXXXXXXXX/mem";
		sprintf(mem_str, "/proc/%i/mem", child);
		child_mem = open(mem_str, O_RDWR | O_CLOEXEC);
		if (-1 == child_mem) {
			fprintf(stderr, "open: %s",
			        ntstatus_str(errno_to_ntstatus(errno)));
			return EXIT_FAILURE;
		}
	}

	{
		sigset_t signals;
		sigemptyset(&signals);
		sigaddset(&signals, SIGCHLD);

		err = pthread_sigmask(SIG_BLOCK, &signals, 0);
		if (err != 0) {
			errno = err;
			fprintf(stderr, "pthread_sigmask: %s",
			        ntstatus_str(errno_to_ntstatus(errno)));
			return EXIT_FAILURE;
		}
	}

	int signal_fd;
	{
		sigset_t signals;
		sigemptyset(&signals);
		sigaddset(&signals, SIGCHLD);

		signal_fd =
		    signalfd(-1, &signals, SFD_CLOEXEC | SFD_NONBLOCK);
	}
	if (-1 == signal_fd) {
		fprintf(stderr, "signalfd: %s",
		        ntstatus_str(errno_to_ntstatus(errno)));
		return EXIT_FAILURE;
	}

	int thread_exit_fd = eventfd(0U, EFD_CLOEXEC);
	if (-1 == thread_exit_fd) {
		fprintf(stderr, "eventfd: %s",
		        ntstatus_str(errno_to_ntstatus(errno)));
		return EXIT_FAILURE;
	}

	struct process *child_proc = calloc(1U, sizeof *child_proc);
	if (0 == child_proc) {
		fprintf(stderr, "calloc: %s",
		        ntstatus_str(errno_to_ntstatus(errno)));
		return EXIT_FAILURE;
	}
	child_proc->pid = child;
	child_proc->exit_fd = thread_exit_fd;
	child_proc->has_pending_child_signal = 0;
	child_proc->mem_fd = child_mem;
	child_proc->binary_name = binary_name;
	child_proc->shared_pages = shared_pages;
	child_proc->anonymous_memory = anonymous_memory;
	child_proc->syscall_context_not_set = true;
	child_proc->syscall_result = 0;

	child_proc->handle_table[0U] = stdin_handle;
	child_proc->handle_table[1U] = stdout_handle,
	child_proc->handle_table[2U] = stderr_handle;

	pthread_t process_thread;
	{
		pthread_attr_t attr;

		err = pthread_attr_init(&attr);
		assert(0 == err);

		err = pthread_attr_setdetachstate(
		    &attr, PTHREAD_CREATE_DETACHED);
		assert(0 == err);

		{
			pthread_t xx;
			err = pthread_create(
			    &xx, 0, process_thread_routine, child_proc);
			if (err != 0) {
				fprintf(stderr, "pthread_create: %s",
				        ntstatus_str(
				            errno_to_ntstatus(err)));
				return EXIT_FAILURE;
			}
			process_thread = xx;
		}
		err = pthread_attr_destroy(&attr);
		assert(0 == err);
	}

	for (;;) {
		struct pollfd pollfds[2U];

		pollfds[0U].fd = signal_fd;
		pollfds[0U].events = POLLIN;

		pollfds[1U].fd = thread_exit_fd;
		pollfds[1U].events = POLLIN;

		int numpolled = poll(pollfds, 2U, -1);
		if (-1 == numpolled) {
			err = errno;
			assert(err != 0);
			if (EINTR == err)
				continue;
			if (EAGAIN == err)
				continue;
			fprintf(stderr, "poll: %s",
			        ntstatus_str(errno_to_ntstatus(errno)));
			return EXIT_FAILURE;
		}

		size_t ii;
		for (ii = 0U; ii < 2U; ++ii) {
			if (numpolled <= 0)
				break;

			bool has_pollerr =
			    (pollfds[ii].revents & POLLERR) != 0;
			bool has_pollhup =
			    (pollfds[ii].revents & POLLHUP) != 0;
			bool has_pollnval =
			    (pollfds[ii].revents & POLLNVAL) != 0;
			bool has_pollin =
			    (pollfds[ii].revents & POLLIN) != 0;

			assert(!has_pollerr);
			assert(!has_pollhup);
			assert(!has_pollnval);

			if (!has_pollin)
				continue;

			--numpolled;

			switch (ii) {
			case 0: {
				struct signalfd_siginfo info = {0};
				int fd = pollfds[ii].fd;
				for (;;) {
					for (;;) {
						if (-1 ==
						    read(fd, &info,
						         sizeof info)) {
							err = errno;
							assert(err !=
							       0);
							if (EINTR ==
							    err)
								continue;
							if (EAGAIN ==
							    err)
								goto finish_signals;
							fprintf(
							    stderr,
							    "read: %s",
							    ntstatus_str(
							        errno_to_ntstatus(
							            errno)));
							return EXIT_FAILURE;
						}
						break;
					}

					switch (info.ssi_signo) {
					case SIGCHLD:
						__atomic_store_n(
						    &child_proc
						         ->has_pending_child_signal,
						    1U,
						    __ATOMIC_RELEASE);

						wakeup_process(
						    child_proc);
						break;

					default:
						continue;
					}
				}
			finish_signals:
				break;
			}

			case 1: {
				int fd = pollfds[ii].fd;
				uint64_t exit_value;
				for (;;) {
					uint64_t xx;
					if (-1 ==
					    read(fd, &xx, sizeof xx)) {
						err = errno;
						assert(err != 0);
						if (EINTR == err)
							continue;
						fprintf(
						    stderr, "read: %s",
						    ntstatus_str(
						        errno_to_ntstatus(
						            errno)));
						return EXIT_FAILURE;
					}
					exit_value = xx;
					break;
				}
				fprintf(stderr, "exit value: %lu\n",
				        exit_value);
				goto join_thread;
			}
			}
		}
	}

join_thread:
	return EXIT_SUCCESS;
}

static int child_func(void *arg)
{
	struct handle *anonymous_memory =
	    ((struct child_func_args *)arg)->anonymous_memory;
	if (MAP_FAILED ==
	    mmap((void *)&_WUM_KERNEL_ABI_SHARED_PAGESP->execute_only,
	         sizeof _WUM_KERNEL_ABI_SHARED_PAGESP->execute_only,
	         PROT_EXEC, MAP_FIXED | MAP_SHARED,
	         anonymous_memory->fd,
	         (uint64_t)&_WUM_KERNEL_ABI_SHARED_PAGESP
	             ->execute_only)) {
		perror("mmap");
		_Exit(EXIT_FAILURE);
	}

	{
		struct rlimit limit;
		limit.rlim_cur = 0;
		limit.rlim_max = 0;

		/* Prevent opening files */
		if (-1 == setrlimit(RLIMIT_NOFILE, &limit)) {
			perror("setrlimit");
			_Exit(EXIT_FAILURE);
		}

		/* Prevent forking */
		if (-1 == setrlimit(RLIMIT_NPROC, &limit)) {
			perror("setrlimit");
			_Exit(EXIT_FAILURE);
		}
	}

	raise(SIGSTOP);

	abort();
}

static uint64_t real_process_thread_routine(struct process *process);
static void start_main_loop(struct process *process);

static void *process_thread_routine(void *arg)
{
	int err = 0;
	struct process *process;
	int exit_fd;

	process = arg;
	exit_fd = process->exit_fd;

	uint64_t exit_value = real_process_thread_routine(process);

	for (;;) {
		uint64_t xx = 0xFF;
		if (-1 == write(exit_fd, &xx, sizeof xx)) {
			err = errno;
			assert(err != 0);
			if (EINTR == err)
				continue;

			assert(false);
		}
		break;
	}

	return 0;
}

static int handle_signal(struct process *process,
                         siginfo_t const *info);
static void handle_system_call(struct process *process);

static uint64_t real_process_thread_routine(struct process *process)
{
	int err = 0;
	struct _wum_kernel_abi_shared_pages *shared_pages;
	uint32_t exit_value = 0U;

	shared_pages = process->shared_pages;
	shared_pages->read_write.value.process_parameters.hStdInput =
	    (__wum_kernel_HANDLE)(4096U + (0U << 2U));
	shared_pages->read_write.value.process_parameters.hStdOutput =
	    (__wum_kernel_HANDLE)(4096U + (1U << 2U));
	shared_pages->read_write.value.process_parameters.hStdError =
	    (__wum_kernel_HANDLE)(4096U + (2U << 2U));
	shared_pages->read_write.value.peb.ProcessParameters =
	    (void *)(_WUM_KERNEL_ABI_SHARED_PAGES +
	             offsetof(
	                 struct _wum_kernel_abi_read_write_page_value,
	                 process_parameters));

	err = ptrace_seize(process->pid,
	                   PTRACE_O_EXITKILL | PTRACE_O_TRACESYSGOOD);
	if (err != 0)
		goto kill_process;

	void *syscall_stack = mmap(
	    0, 20U * 4096U, PROT_READ | PROT_WRITE,
	    MAP_PRIVATE | MAP_STACK | MAP_ANONYMOUS | MAP_GROWSDOWN, -1,
	    0);
	if (MAP_FAILED == syscall_stack) {
		err = errno;
		goto kill_process;
	}
	mprotect(syscall_stack, 4096U, PROT_NONE);
	mprotect(((char *)syscall_stack) + 19U * 4096U, 4096U,
	         PROT_NONE);

	getcontext(&process->normal_context);

	process->syscall_context = process->normal_context;
	process->syscall_context.uc_link = &process->normal_context;
	process->syscall_context.uc_stack.ss_sp =
	    ((char *)syscall_stack) + 4096U;
	process->syscall_context.uc_stack.ss_size = 18U * 4096U;

	process->syscall_context_not_set = false;
	makecontext(&process->syscall_context,
	            (void (*)(void))start_main_loop, 1U, process);
	swapcontext(&process->normal_context,
	            &process->syscall_context);

	for (;;) {
		if (check_and_clear_flag(&process->have_pending_exit)) {
			__atomic_thread_fence(__ATOMIC_ACQUIRE);

			exit_value = process->pending_exit_value;
			break;
		}

		if (check_and_clear_flag(
		        &process->has_pending_child_signal)) {
			__atomic_thread_fence(__ATOMIC_ACQUIRE);

			for (;;) {
				siginfo_t wait_info = {0};
				if (-1 == waitid(P_PID, process->pid,
				                 &wait_info,
				                 WEXITED | WNOHANG)) {
					err = errno;
					assert(err != 0);
					return err;
				}
				if (0 == wait_info.si_pid)
					break;

				err =
				    handle_signal(process, &wait_info);
				if (err != 0)
					break;
			}
			if (err != 0)
				break;
		}

		if (check_and_clear_flag(
		        &shared_pages->read_write.value
		             .have_pending_system_call)) {
			__atomic_thread_fence(__ATOMIC_ACQUIRE);
			getcontext(&process->normal_context);

			process->syscall_context =
			    process->normal_context;
			process->syscall_context.uc_link =
			    &process->normal_context;
			process->syscall_context.uc_stack.ss_sp =
			    ((char *)syscall_stack) + 4096U;
			process->syscall_context.uc_stack.ss_size =
			    18U * 4096U;

			process->syscall_context_not_set = false;
			makecontext(&process->syscall_context,
			            (void (*)(void))handle_system_call,
			            1U, process);
			swapcontext(&process->normal_context,
			            &process->syscall_context);

			err = process->syscall_result;
			if (err != 0)
				break;
		}

		for (;;) {
			unsigned char ii;
			for (ii = 0U; ii < 20U; ++ii) {
				if (check_and_clear_flag(
				        &shared_pages->read_write.value
				             .wakeup_kernel_thread))
					goto poll_wakeupers;

				WUM_PAUSE();
			}

			_wum_kernel_futex_wait(
			    &shared_pages->read_write.value
			         .wakeup_kernel_thread,
			    0, 0);
		}
	poll_wakeupers:
		;
	}
kill_process:
	if (ECANCELED == err)
		err = 0U;

	return err;
}

static void complete_syscall(struct process *process, NTSTATUS status);

static int process_load_library(struct process *process,
                                char const *binary_name,
                                uint64_t *entry_pointp)
{
	int err = 0;

	char const *path = getenv("PATH");

	int binary_fd;
	for (char const *ii = path;;) {
		char const *end = strchr(ii, ':');

		size_t len;
		if (0 == end) {
			len = strlen(ii);
		} else {
			len = end - ii;
		}

		char *possible_path = strndup(ii, len);
		if (0 == possible_path) {
			err = errno;
			assert(err != 0);
			goto complete;
		}

		char *possible_binary;
		{
			char *xx;
			if (asprintf(&xx, "%s/%s", possible_path,
			             binary_name) < 0) {
				err = errno;
				assert(err != 0);
				goto free_path;
			}
			possible_binary = xx;
		}

		binary_fd = open(possible_binary, O_RDONLY | O_CLOEXEC);
		if (-1 == binary_fd) {
			err = errno;
			assert(err != 0);
		}

		free(possible_binary);

	free_path:
		free(possible_path);

		if (0 == err) {
			goto found_binary;
		} else if (ENOENT == err || EACCES == err) {
			err = 0;
		} else {
			goto complete;
		}

		if (0 == end)
			break;
		ii = end + 1U;
	}

	err = ENOENT;
	goto complete;

found_binary:
	;
	IMAGE_DOS_HEADER image_dos_header = {0};
	if (-1 == pread(binary_fd, &image_dos_header,
	                sizeof image_dos_header, 0)) {
		err = errno;
		assert(err != 0);
		goto complete;
	}

	if (image_dos_header.e_magic != 0x05A4D) {
		fprintf(stderr, "not DOS file: 0%X\n",
		        image_dos_header.e_magic);
		goto complete;
	}

	off_t pe_offset = image_dos_header.e_lfanew;

	uint16_t win_bytes = {0};
	if (-1 ==
	    pread(binary_fd, &win_bytes, sizeof win_bytes, pe_offset)) {
		err = errno;
		assert(err != 0);
		goto complete;
	}
	if (win_bytes != 0x00004550) {
		fprintf(stderr, "not a windows file: 0x%X\n",
		        win_bytes);
		goto complete;
	}

	IMAGE_FILE_HEADER image_file_header = {0};
	if (-1 == pread(binary_fd, &image_file_header,
	                IMAGE_FILE_HEADER_SIZE, pe_offset + 4U)) {
		err = errno;
		assert(err != 0);
		goto complete;
	}
	off_t number_of_sections = image_file_header.NumberOfSections;

	IMAGE_OPTIONAL_HEADER image_optional_header = {0};
	if (-1 == pread(binary_fd, &image_optional_header,
	                IMAGE_OPTIONAL_HEADER_SIZE,
	                pe_offset + 4U + IMAGE_FILE_HEADER_SIZE)) {
		err = errno;
		assert(err != 0);
		goto complete;
	}

	if (image_optional_header.Magic != 0x020b) {
		fprintf(stderr, "not a windows file: 0x%X\n",
		        image_optional_header.Magic);
		goto complete;
	}

	off_t image_base_offset = image_optional_header.ImageBase;
	off_t image_size = image_optional_header.SizeOfImage;
	off_t entry_point = image_optional_header.AddressOfEntryPoint +
	                    image_base_offset;
	off_t headers_size = image_optional_header.SizeOfHeaders;
	fprintf(stderr, "image base: 0x%lX 0x%lx 0x%lX\n",
	        image_base_offset, image_size, entry_point);

	err =
	    process_mmap(process, 0, image_base_offset,
	                 headers_size + (4096U - headers_size % 4096U),
	                 PROT_READ | PROT_WRITE,
	                 MAP_ANONYMOUS | MAP_PRIVATE | MAP_FIXED, 0, 0);
	if (err != 0) {
		err = errno;
		assert(err != 0);
		goto complete;
	}

	err = proxy_pread(process, 0, binary_fd, image_base_offset,
	                  headers_size, 0U);
	if (err != 0) {
		err = errno;
		assert(err != 0);
		goto complete;
	}

	err = process_mprotect(
	    process, image_base_offset,
	    headers_size + (4096U - headers_size % 4096U), PROT_READ);
	if (err != 0) {
		err = errno;
		assert(err != 0);
		goto complete;
	}

	off_t headers_start = image_base_offset + pe_offset + 4U +
	                      IMAGE_FILE_HEADER_SIZE +
	                      IMAGE_OPTIONAL_HEADER_SIZE;

	for (off_t ii = 0U; ii < number_of_sections; ++ii) {
		IMAGE_SECTION_HEADER header = {0};
		if (-1 == pread(process->mem_fd, &header, sizeof header,
		                headers_start +
		                    ii * sizeof(IMAGE_SECTION_HEADER)))
			goto complete;

		char name[9U] = {0};
		snprintf(name, 8U, "%s", header.Name);

		off_t virtual_address =
		    image_base_offset + header.VirtualAddress;
		off_t file_size = header.SizeOfRawData;
		off_t memsz = header.Misc.VirtualSize;
		off_t file_offset = header.PointerToRawData;

		uint32_t characteristics = header.Characteristics;

		bool discardable =
		    (characteristics & IMAGE_SCN_MEM_DISCARDABLE) != 0U;
		bool code_section =
		    (characteristics & IMAGE_SCN_CNT_CODE) != 0U;
		bool initialized =
		    (characteristics &
		     IMAGE_SCN_CNT_INITIALIZED_DATA) != 0U;
		bool uninitialized =
		    (characteristics &
		     IMAGE_SCN_CNT_UNINITIALIZED_DATA) != 0U;
		bool uncacheable =
		    (characteristics & IMAGE_SCN_MEM_NOT_CACHED) != 0U;
		bool nonpageable =
		    (characteristics & IMAGE_SCN_MEM_NOT_PAGED) != 0U;
		bool shared =
		    (characteristics & IMAGE_SCN_MEM_SHARED) != 0U;
		bool executable =
		    (characteristics & IMAGE_SCN_MEM_EXECUTE) != 0U;
		bool readable =
		    (characteristics & IMAGE_SCN_MEM_READ) != 0U;
		bool writable =
		    (characteristics & IMAGE_SCN_MEM_WRITE) != 0U;

		fprintf(stderr, "name: %s 0x%lX 0x%lX 0x%lX "
		                "0x%lX%s%s%s%s%s%s%s%s%s%s\n",
		        name, virtual_address, memsz, file_size,
		        file_offset, discardable ? " discardable" : "",
		        code_section ? " code" : "",
		        initialized ? " initialized" : "",
		        uninitialized ? " uninitialized" : "",
		        uncacheable ? " uncacheable" : "",
		        nonpageable ? " nonpageable" : "",
		        shared ? " shared" : "",
		        executable ? " executable" : "",
		        readable ? " readable" : "",
		        writable ? " writable" : "");

		size_t filler_bottom = virtual_address % 4096U;
		size_t aligned_bottom = virtual_address - filler_bottom;

		size_t filler_top = 4096U - memsz % 4096U;
		size_t aligned_size = memsz + filler_top;

		if (discardable)
			continue;

		int prot_flags = PROT_WRITE;

		if (executable)
			prot_flags |= PROT_EXEC;

		if (readable)
			prot_flags |= PROT_READ;

		if (writable)
			prot_flags |= PROT_WRITE;

		off_t aligned_file_offset =
		    (file_offset / 0x200U) * 0x200U;

		if (uninitialized) {
			err = process_mmap(process, 0, aligned_bottom,
			                   aligned_size, prot_flags,
			                   MAP_PRIVATE | MAP_FIXED |
			                       MAP_ANONYMOUS,
			                   0, 0U);
			if (err != 0)
				goto complete;
		} else {
			err = process_mmap(
			    process, 0, aligned_bottom, aligned_size,
			    PROT_READ | PROT_WRITE,
			    MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS, 0,
			    0U);
			if (err != 0)
				goto complete;

			err = proxy_pread(process, 0, binary_fd,
			                  virtual_address, file_size,
			                  aligned_file_offset);
			if (err != 0)
				goto complete;

			err =
			    process_mprotect(process, aligned_bottom,
			                     aligned_size, prot_flags);
			if (err != 0)
				goto complete;
		}
	}
complete:
	*entry_pointp = entry_point;
	return err;
}

static void start_main_loop(struct process *process)
{
	int err = 0;

	char const *binary_name;

	binary_name = process->binary_name;

	err = proxy_brk(process, 0);
	if (err != 0)
		goto complete;

	size_t top = _WUM_KERNEL_ABI_SHARED_PAGES +
	             _WUM_KERNEL_ABI_SHARED_PAGES_SIZE;
	if (ADDRESS_SPACE_TOP - top != 0) {
		err =
		    proxy_munmap(process, top, ADDRESS_SPACE_TOP - top);
		if (err != 0)
			goto complete;
	}

	err = proxy_munmap(process, 0,
	                   (uint64_t)_WUM_KERNEL_ABI_SHARED_PAGES);
	if (err != 0)
		goto complete;

	err = process_mmap(
	    process, 0,
	    (uint64_t)&_WUM_KERNEL_ABI_SHARED_PAGESP->read_write,
	    sizeof _WUM_KERNEL_ABI_SHARED_PAGESP->read_write,
	    PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0U);
	if (err != 0)
		goto complete;

	err = process_mmap(
	    process, 0,
	    (uint64_t)&_WUM_KERNEL_ABI_SHARED_PAGESP->read_only,
	    sizeof _WUM_KERNEL_ABI_SHARED_PAGESP->read_only, PROT_READ,
	    MAP_PRIVATE | MAP_ANONYMOUS, 0, 0U);
	if (err != 0)
		goto complete;

	size_t stack_size = 20U * 4096U;
	uint64_t stack_start;
	{
		uint64_t xx;
		err = process_mmap(process, &xx, 0, stack_size,
		                   PROT_READ | PROT_WRITE,
		                   MAP_STACK | MAP_GROWSDOWN |
		                       MAP_PRIVATE | MAP_ANONYMOUS,
		                   0, 0U);
		if (err != 0)
			goto complete;
		stack_start = xx;
	}
	uint64_t stack_end = stack_start + stack_size;

	err = process_mprotect(process, stack_end - 4096U, 4096U,
	                       PROT_NONE);
	if (err != 0)
		goto complete;

	err = process_mprotect(process, stack_start, 4096U, PROT_NONE);
	if (err != 0)
		goto complete;

	stack_end -= 4096U;

	uint64_t entry_point;
	err = process_load_library(process, binary_name, &entry_point);
	if (err != 0)
		goto complete;

	uint64_t tib_page;
	{
		uint64_t xx;
		err = process_mmap(process, &xx, 0, 4096U,
		                   PROT_READ | PROT_WRITE,
		                   MAP_PRIVATE | MAP_ANONYMOUS, 0, 0U);
		if (err != 0)
			goto complete;
		tib_page = xx;
	}

	{
		uint64_t xx = tib_page;
		err = process_memcpy(
		    process, tib_page + offsetof(struct _wum_kernel_TEB,
		                                 Tib.Self),
		    &xx, sizeof xx);
		if (err != 0)
			goto complete;
	}

	{
		uint64_t xx = 0x77fbe900U;
		err = process_memcpy(
		    process,
		    tib_page + offsetof(struct _wum_kernel_TEB, Peb),
		    &xx, sizeof xx);
		if (err != 0)
			goto complete;
	}

	{
		struct user_regs_struct registers = {0};
		struct user_regs_struct orig_registers = {0};

		struct user_fpregs_struct fpregisters = {0};
		struct user_fpregs_struct orig_fpregisters = {0};

		err = ptrace_getregs(process->pid, &orig_registers);
		if (err != 0)
			goto complete;

		err = ptrace_getfpregs(process->pid, &orig_fpregisters);
		if (err != 0)
			goto complete;

		registers.cs = orig_registers.cs;
		registers.ss = orig_registers.ss;

		registers.fs_base = tib_page;

		registers.rip = entry_point;

		/* Align for SSE */
		registers.rsp = stack_end - 8U;

		err = ptrace_setregs(process->pid, &registers);
		if (err != 0)
			goto complete;

		err = ptrace_setfpregs(process->pid, &fpregisters);
		if (err != 0)
			goto complete;
	}

	fprintf(stderr, "\n");

	print_mmap_info(process);

	fprintf(stderr, "starting: %i\n", process->pid);
	getchar();

	err = ptrace_cont(process->pid, 0);

complete:
	process->syscall_result = err;
}

static void print_mmap_info(struct process *process)
{
	int err = 0;

	fprintf(stderr, "mem map info\n");

	struct vma_index iip = get_lowest_vma(&process->vmas);
	for (; index_is_valid(&process->vmas, iip);
	     iip = get_next_vma(&process->vmas, iip)) {
		uint64_t offset;
		uint64_t addr_bottom;
		uint64_t addr_top;
		bool readable;
		bool writable;
		bool executable;
		bool shared;
		struct handle *handle;

		char const *name;
		int fd;

		struct vma *ii;

		ii = get_vma(&process->vmas, iip);

		offset = ii->offset;
		addr_bottom = ii->addr_bottom;
		addr_top = ii->addr_top;
		readable = ii->readable;
		writable = ii->writable;
		executable = ii->executable;
		shared = ii->shared;

		handle = ii->handle;

		name = handle->name;
		fd = handle->fd;

		dev_t dev;
		ino_t ino;
		{
			struct stat stat = {0};
			if (-1 == fstat(fd, &stat)) {
				err = errno;
				assert(err != 0);
			}
			dev = stat.st_dev;
			ino = stat.st_ino;
		}

		fprintf(stderr, "0x%012lx-0x%012lx %c%c%c%c "
		                "0x%012lx %02u:%02u %lu %s\n",
		        addr_bottom, addr_top, readable ? 'r' : '-',
		        writable ? 'w' : '-', executable ? 'x' : '-',
		        shared ? 's' : 'p', offset, major(dev),
		        minor(dev), ino, name != 0 ? name : "");
	}

	fprintf(stderr, "\n");
}

static int handle_signal(struct process *process,
                         siginfo_t const *wait_info)
{
	int err = 0;
	int wait_signo;
	int wait_signal_errno;
	int wait_code;

	pid_t wait_pid;
	uid_t wait_uid;
	int wait_status;
	clock_t wait_utime;
	clock_t wait_stime;

	wait_signo = wait_info->si_signo;
	wait_signal_errno = wait_info->si_errno;
	wait_code = wait_info->si_code;

	wait_pid = wait_info->si_pid;
	wait_uid = wait_info->si_uid;
	wait_status = wait_info->si_status;
	wait_utime = wait_info->si_utime;
	wait_stime = wait_info->si_stime;

	switch (wait_code) {
	case CLD_DUMPED:
	case CLD_KILLED:
	case CLD_EXITED:
		return ECANCELED;

	case CLD_TRAPPED:
		goto got_trap;

	default:
		assert(false);
		break;
	}

got_trap:
	;

	int ptrace_event = wait_status >> 8U;
	switch (ptrace_event) {
	case 0: {
		int signo = wait_status & 0xFF;
		if (signo == (SIGTRAP | 0x80))
			goto on_ptracee_received_interrupt;

		goto on_ptracee_received_signal;
	}

	case PTRACE_EVENT_STOP:
		goto on_ptracee_received_interrupt;

	default:
		assert(false);
		break;
	}

on_ptracee_received_signal:
	;
	siginfo_t info = {0};
	err = ptrace_getsiginfo(process->pid, &info);
	if (err != 0)
		return err;

	int signo;
	int signal_errno;
	int code;

	pid_t pid;
	uid_t uid;
	int status;
	clock_t utime;
	clock_t stime;
	int int_value;
	uint64_t ptr_value;
	int overrun;
	int timerid;
	uint64_t addr;
	long band;

	int fd;
	short addr_lsb;

	signo = info.si_signo;
	signal_errno = info.si_errno;
	code = info.si_code;

	fprintf(stderr, "signal!\n"
	                "\tsigno: %" PRIuMAX "\n"
	                "\terrno: %" PRIuMAX "\n"
	                "\tcode: %" PRIuMAX "\n"
	                "\tpid: %" PRIuMAX "\n"
	                "\tuid: %" PRIuMAX "\n"
	                "\tstatus: %" PRIuMAX "\n"
	                "\tutime: %" PRIuMAX "\n"
	                "\tstime: %" PRIuMAX "\n",
	        (uintmax_t)wait_signo, (uintmax_t)wait_signal_errno,
	        (uintmax_t)wait_code, (uintmax_t)wait_pid,
	        (uintmax_t)wait_uid, (uintmax_t)wait_status,
	        (uintmax_t)wait_utime, (uintmax_t)wait_stime);

	fprintf(stderr, "signal!\n"
	                "\tsigno: %" PRIuMAX "\n"
	                "\terrno: %" PRIuMAX "\n"
	                "\tcode: %" PRIuMAX "\n",
	        (uintmax_t)signo, (uintmax_t)signal_errno,
	        (uintmax_t)code);

	switch (code) {
	case SI_USER:
	case SI_TKILL:
		pid = info.si_pid;
		uid = info.si_uid;

		fprintf(stderr, "\tpid: %" PRIuMAX "\n"
		                "\tuid: %" PRIuMAX "\n",
		        (uintmax_t)pid, (uintmax_t)uid);
		break;

	case SI_KERNEL:
	default:
		switch (signo) {
		case SIGILL:
		case SIGFPE:
		case SIGSEGV:
		case SIGBUS:
			addr = (uint64_t)info.si_addr;
			addr_lsb = info.si_addr_lsb;
			fprintf(stderr, "\taddr: 0x%" PRIxMAX "\n"
			                "\taddr_lsb: %" PRIuMAX "\n",
			        (uintmax_t)addr, (uintmax_t)addr_lsb);
			break;

		case SIGCHLD:
			pid = info.si_pid;
			uid = info.si_uid;
			status = info.si_status;
			utime = info.si_utime;
			stime = info.si_stime;
			fprintf(stderr, "\tpid: %" PRIuMAX "\n"
			                "\tuid: %" PRIuMAX "\n"
			                "\tstatus: %" PRIuMAX "\n"
			                "\tutime: %" PRIuMAX "\n"
			                "\tstime: %" PRIuMAX "\n",
			        (uintmax_t)pid, (uintmax_t)uid,
			        (uintmax_t)status, (uintmax_t)utime,
			        (uintmax_t)stime);
			break;

		case SIGPOLL:
			band = info.si_band;
			fd = info.si_fd;
			fprintf(stderr, "\tband: %" PRIuMAX "\n"
			                "\tfd: %" PRIuMAX "\n",
			        (uintmax_t)band, (uintmax_t)fd);
			break;
		}
		break;

	case SI_QUEUE:
	case SI_MESGQ:
	case SI_ASYNCIO:
		pid = info.si_pid;
		uid = info.si_uid;

		int_value = info.si_int;
		ptr_value = (uint64_t)info.si_ptr;

		fprintf(stderr, "\tpid: %" PRIuMAX "\n"
		                "\tuid: %" PRIuMAX "\n"
		                "\tint_value: %" PRIuMAX "\n"
		                "\tptr_value: 0x%" PRIxMAX "\n",
		        (uintmax_t)pid, (uintmax_t)uid,
		        (uintmax_t)int_value, (uintmax_t)ptr_value);
		break;

	case SI_TIMER:
		overrun = info.si_overrun;
		timerid = info.si_timerid;

		int_value = info.si_int;
		ptr_value = (uint64_t)info.si_ptr;
		fprintf(stderr, "\toverrun: %" PRIuMAX "\n"
		                "\ttimerid: %" PRIuMAX "\n"
		                "\tint_value: %" PRIuMAX "\n"
		                "\tptr_value: 0x%" PRIxMAX "\n",
		        (uintmax_t)overrun, (uintmax_t)timerid,
		        (uintmax_t)int_value, (uintmax_t)ptr_value);
		break;
	}

	err = ptrace_cont(process->pid, signo);
	if (err != 0)
		return err;

	switch (wait_signo) {
	case 0:
	case SIGCONT:
	case SIGTRAP:
		break;

	default:
		push_exit_event(process, signo);
		return 0;
	}
	return 0;

on_ptracee_received_interrupt:
	if (process->syscall_context_not_set)
		return 0;
	swapcontext(&process->normal_context,
	            &process->syscall_context);
	err = process->syscall_result;
	if (err != 0)
		return err;
	return 0;
}

static void complete_syscall(struct process *process, NTSTATUS status)
{
	struct _wum_kernel_abi_shared_pages *shared_pages =
	    process->shared_pages;

	shared_pages->read_only.value.error = status;

	__atomic_thread_fence(__ATOMIC_RELEASE);

	set_and_wake_flag(
	    &shared_pages->read_write.value.syscall_completed);
}

static void handle_system_call(struct process *process)
{
	struct _wum_kernel_abi_shared_pages *shared_pages =
	    process->shared_pages;

	uint64_t arg1 = shared_pages->read_write.value.arguments[0U];
	uint64_t arg2 = shared_pages->read_write.value.arguments[1U];
	uint64_t arg3 = shared_pages->read_write.value.arguments[2U];
	uint64_t arg4 = shared_pages->read_write.value.arguments[3U];
	uint64_t arg5 = shared_pages->read_write.value.arguments[4U];
	uint64_t arg6 = shared_pages->read_write.value.arguments[5U];
	uint64_t arg7 = shared_pages->read_write.value.arguments[6U];
	uint64_t arg8 = shared_pages->read_write.value.arguments[7U];
	uint64_t arg9 = shared_pages->read_write.value.arguments[8U];

	uint64_t system_call =
	    shared_pages->read_write.value.system_call;

	int err = 0U;
	switch (system_call) {
	case _WUM_KERNEL_ABI_SYSCALL_NT_ALLOCATE_VIRTUAL_MEMORY:
		err = nt_allocate_virtual_memory(
		    process, arg1, arg2, arg3, arg4, arg5, arg6);
		break;

	case _WUM_KERNEL_ABI_SYSCALL_NT_FREE_VIRTUAL_MEMORY:
		err = nt_free_virtual_memory(process, arg1, arg2, arg3,
		                             arg4);
		break;

	case _WUM_KERNEL_ABI_SYSCALL_NT_WRITE_FILE:
		err = nt_write_file(process, arg1, arg2, arg3, arg4,
		                    arg5, arg6, arg7, arg8, arg9);
		break;

	case _WUM_KERNEL_ABI_SYSCALL_NT_CLOSE:
		err = nt_close(process, arg1);
		break;

	case _WUM_KERNEL_ABI_SYSCALL_NT_DUPLICATE_OBJECT:
		err = nt_duplicate_object(process, arg1, arg2, arg3,
		                          arg4, arg5, arg6, arg7);
		break;

	case _WUM_KERNEL_ABI_SYSCALL_NT_TERMINATE_PROCESS: {
		uint64_t pid = arg1;
		uint64_t exit_code = arg2;

		if (pid != (CURRENT_PROCESS_HANDLE)) {
			complete_syscall(
			    process,
			    _WUM_KERNEL_STATUS_INVALID_PARAMETER);
			break;
		}

		push_exit_event(process, exit_code);
		break;
	}

	default:
		fprintf(stderr, "syscall %" PRIu64 "\n", system_call);
		break;
	}

	process->syscall_result = err;
}

static void push_exit_event(struct process *process,
                            uint32_t exit_value)
{
	process->pending_exit_value = exit_value;
	__atomic_thread_fence(__ATOMIC_RELEASE);
	kill(process->pid, SIGKILL);
}

static void wakeup_process(struct process *process)
{
	struct _wum_kernel_abi_shared_pages *shared_pages =
	    process->shared_pages;

	set_and_wake_flag(
	    &shared_pages->read_write.value.wakeup_kernel_thread);
}

static bool check_and_clear_flag(uint32_t volatile *var)
{
	uint32_t expected = 1;
	return __atomic_compare_exchange_n(var, &expected, 0, true,
	                                   __ATOMIC_SEQ_CST,
	                                   __ATOMIC_SEQ_CST);
}

static void set_and_wake_flag(uint32_t volatile *var)
{
	__atomic_store_n(var, 1U, __ATOMIC_RELEASE);

	/* In the best case avoid a system call */
	if (!__atomic_load_n(var, __ATOMIC_ACQUIRE))
		return;

	_wum_kernel_futex_wake(0, var, 1U);
}

static int stop_process(struct process *process)
{
	int err = 0;

	err = ptrace_interrupt(process->pid);
	if (err != 0)
		return err;
	swapcontext(&process->syscall_context,
	            &process->normal_context);
	return 0;
}

static int nt_allocate_virtual_memory(
    struct process *process, uint64_t process_handle,
    uint64_t base_addressp, uint64_t zero_bits, uint64_t region_sizep,
    uint64_t allocation_type, uint64_t protect)
{
	NTSTATUS error = 0;

	/* Not Implemented */
	if (process_handle != CURRENT_PROCESS_HANDLE) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	/* Would Fault */
	if (0U == base_addressp) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	if (zero_bits != 0U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	if (0U == region_sizep) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t all_mem_flags =
	    _WUM_KERNEL_MEM_COMMIT | _WUM_KERNEL_MEM_PHYSICAL |
	    _WUM_KERNEL_MEM_RESERVE | _WUM_KERNEL_MEM_RESET |
	    _WUM_KERNEL_MEM_TOP_DOWN;

	if ((allocation_type & ~all_mem_flags) != 0U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	bool commit = (allocation_type & _WUM_KERNEL_MEM_COMMIT) != 0U;
	bool physical =
	    (allocation_type & _WUM_KERNEL_MEM_PHYSICAL) != 0U;
	bool reserve =
	    (allocation_type & _WUM_KERNEL_MEM_RESERVE) != 0U;
	bool reset = (allocation_type & _WUM_KERNEL_MEM_RESET) != 0U;
	bool top_down =
	    (allocation_type & _WUM_KERNEL_MEM_TOP_DOWN) != 0U;

	/* I don't know how to implement this yet */
	if (top_down) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	/* I don't know how to implement this yet */
	if (reset) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	/* I don't know how to implement this yet */
	if (reserve) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	unsigned char mem_flags_count = 0U;
	if (commit)
		++mem_flags_count;

	if (physical)
		++mem_flags_count;

	if (reserve)
		++mem_flags_count;

	if (mem_flags_count != 1U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t all_prot_flags =
	    _WUM_KERNEL_PAGE_NOACCESS | _WUM_KERNEL_PAGE_READONLY |
	    _WUM_KERNEL_PAGE_READWRITE | _WUM_KERNEL_PAGE_EXECUTE |
	    _WUM_KERNEL_PAGE_EXECUTE_READ |
	    _WUM_KERNEL_PAGE_EXECUTE_READWRITE |
	    _WUM_KERNEL_PAGE_GUARD | _WUM_KERNEL_PAGE_NOCACHE |
	    _WUM_KERNEL_PAGE_WRITECOMBINE;

	if ((protect & ~all_prot_flags) != 0U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	bool noaccess = (protect & _WUM_KERNEL_PAGE_NOACCESS) != 0U;
	bool readonly = (protect & _WUM_KERNEL_PAGE_READONLY) != 0U;
	bool readwrite = (protect & _WUM_KERNEL_PAGE_READWRITE) != 0U;
	bool execute = (protect & _WUM_KERNEL_PAGE_EXECUTE) != 0U;
	bool execute_read =
	    (protect & _WUM_KERNEL_PAGE_EXECUTE_READ) != 0U;
	bool execute_read_write =
	    (protect & _WUM_KERNEL_PAGE_EXECUTE_READWRITE) != 0U;
	bool guard = (protect & _WUM_KERNEL_PAGE_GUARD) != 0U;
	bool nocache = (protect & _WUM_KERNEL_PAGE_NOCACHE) != 0U;
	bool writecombine =
	    (protect & _WUM_KERNEL_PAGE_WRITECOMBINE) != 0U;

	/* I don't know how to implement this yet */
	if (noaccess || guard || nocache || writecombine) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	unsigned char prot_types_count = 0U;
	if (readonly)
		++prot_types_count;

	if (readwrite)
		++prot_types_count;

	if (execute)
		++prot_types_count;

	if (execute_read)
		++prot_types_count;

	if (execute_read_write)
		++prot_types_count;

	if (prot_types_count != 1U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	int mmap_prot_flags = 0;
	if (readonly) {
		mmap_prot_flags = PROT_READ;
	} else if (readwrite) {
		mmap_prot_flags = PROT_READ | PROT_WRITE;
	} else if (execute) {
		mmap_prot_flags = PROT_EXEC;
	} else if (execute_read) {
		mmap_prot_flags = PROT_EXEC | PROT_READ;
	} else if (execute_read_write) {
		mmap_prot_flags = PROT_EXEC | PROT_READ | PROT_WRITE;
	}

	uint64_t base_address;
	{
		uint64_t xx;
		ssize_t yy = pread(process->mem_fd, &xx, sizeof xx,
		                   base_addressp);
		if (-1 == yy)
			return errno_to_ntstatus(errno);
		base_address = xx;
	}
	if (base_address != 0U) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t region_size;
	{
		uint64_t xx;
		ssize_t yy = pread(process->mem_fd, &xx, sizeof xx,
		                   region_sizep);
		if (-1 == yy)
			return errno_to_ntstatus(errno);
		region_size = xx;
	}
	if (0U == region_size) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	error = stop_process(process);
	if (error != 0)
		goto complete_syscall;

	{
		uint64_t xx;
		error = errno_to_ntstatus(process_mmap(
		    process, &xx, 0, region_size, mmap_prot_flags,
		    MAP_PRIVATE | MAP_ANONYMOUS, 0, 0U));
		if (0U == error)
			base_address = xx;
	}

	int cont_err = ptrace_cont(process->pid, 0);
	if (0U == error)
		error = errno_to_ntstatus(cont_err);

	{
		uint64_t xx = base_address;
		int result = process_memcpy(process, base_addressp, &xx,
		                            sizeof xx);
		if (0 == error)
			error = errno_to_ntstatus(result);
	}

	{
		uint64_t xx = region_size;
		int result = process_memcpy(process, region_sizep, &xx,
		                            sizeof xx);
		if (0 == error)
			error = errno_to_ntstatus(result);
	}

complete_syscall:
	complete_syscall(process, error);
	return 0;
}

static int nt_free_virtual_memory(struct process *process,
                                  uint64_t process_handle,
                                  uint64_t base_addressp,
                                  uint64_t region_sizep,
                                  uint64_t free_type)
{
	NTSTATUS error = 0;

	/* Not Implemented */
	if (process_handle != CURRENT_PROCESS_HANDLE) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	/* Would Fault */
	if (0U == base_addressp) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	if (0U == region_sizep) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t all_mem_flags =
	    _WUM_KERNEL_MEM_DECOMMIT | _WUM_KERNEL_MEM_RELEASE;
	if ((free_type & ~all_mem_flags) != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	bool decommit = (free_type & _WUM_KERNEL_MEM_DECOMMIT) != 0U;
	bool release = (free_type & _WUM_KERNEL_MEM_RELEASE) != 0U;

	if (decommit && release) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	if (!(decommit || release)) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t base_address;
	{
		uint64_t xx;
		ssize_t yy = pread(process->mem_fd, &xx, sizeof xx,
		                   base_addressp);
		if (-1 == yy)
			return errno_to_ntstatus(errno);
		base_address = xx;
	}

	uint64_t region_size;
	{
		uint64_t xx;
		ssize_t yy = pread(process->mem_fd, &xx, sizeof xx,
		                   region_sizep);
		if (-1 == yy)
			return errno_to_ntstatus(errno);
		region_size = xx;
	}
	if (0U == region_size) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	error = stop_process(process);
	if (error != 0)
		goto complete_syscall;

	error = errno_to_ntstatus(
	    process_munmap(process, base_address, region_size));

	int cont_err = ptrace_cont(process->pid, 0);
	if (0 == error)
		error = errno_to_ntstatus(cont_err);

complete_syscall:
	complete_syscall(process, error);
	return 0;
}

static int nt_write_file(struct process *process, uint64_t hFile,
                         uint64_t hEvent, uint64_t apc,
                         uint64_t apc_user, uint64_t io_statusp,
                         uint64_t buffer, uint64_t length,
                         uint64_t offset, uint64_t key)
{
	NTSTATUS error = 0U;

	/* Reserved */
	if (apc != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	if (apc_user != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	if (key != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	/* Written to, shouldn't be zero */
	if (0 == io_statusp) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	if (0 == buffer) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	/* Not implemented yet */
	if (hEvent != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	if (offset != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	struct handle *hFileHandle = process_handle_get(process, hFile);
	if (0 == hFileHandle) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto write_information;
	}

	int real_fd = hFileHandle->fd;
	uint64_t bytes_to_write = length;

	error = stop_process(process);
	if (error != 0)
		goto write_information;

	uint64_t bytes_written = 0U;

	for (;;) {
		size_t bytes_just_wrote;
		{
			uint64_t xx;
			int err = proxy_write(process, &xx, real_fd,
			                      buffer, bytes_to_write);
			if (err != 0) {
				error = errno_to_ntstatus(err);
				break;
			}
			bytes_just_wrote = xx;
		}

		bytes_written += bytes_just_wrote;

		if (bytes_written >= bytes_to_write)
			break;
	}

	int cont_err = ptrace_cont(process->pid, 0);
	if (0 == error)
		error = errno_to_ntstatus(cont_err);

write_information:
	handle_put(hFileHandle);

	char bits[4U + 4U + 8U] = {0};
	{
		int32_t err = error;
		uint64_t info = 0U;

		memcpy(bits, &err, sizeof err);

		memcpy(bits + 8U, &info, sizeof info);
	}

	int result =
	    process_memcpy(process, io_statusp, bits, sizeof bits);
	if (0 == error)
		error = errno_to_ntstatus(result);

	complete_syscall(process, error);
	return 0;
}
static int
nt_duplicate_object(struct process *process,
                    uint64_t SourceProcessHandle, uint64_t SourceHandle,
                    uint64_t TargetProcessHandle, uint64_t TargetHandle,
                    uint64_t DesiredAccess, uint64_t HandleAttributes,
                    uint64_t Options)
{
	NTSTATUS status = 0;

	uint64_t available_options = _WUM_KERNEL_DUPLICATE_SAME_ACCESS;
	if ((Options & ~available_options) != 0U) {
		status = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	bool duplicate_access =
	    (Options & _WUM_KERNEL_DUPLICATE_SAME_ACCESS) != 0U;
	if (!duplicate_access) {
		status = _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;
		goto complete_syscall;
	}

	if (SourceProcessHandle != CURRENT_PROCESS_HANDLE) {
		status = _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;
		goto complete_syscall;
	}

	if (TargetProcessHandle != CURRENT_PROCESS_HANDLE) {
		status = _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;
		goto complete_syscall;
	}

	struct handle *handle =
	    process_handle_get(process, SourceHandle);
	if (0 == handle) {
		status = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t new_handle;
	{
		struct handle **handle_table = process->handle_table;
		size_t empty_spot;
		for (size_t ii = 0U; ii < NUM_HANDLES; ++ii) {
			if (0 == handle_table[ii]) {
				empty_spot = ii;
				goto got_empty_spot;
			}
		}
		handle_put(handle);
		status = _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;
		goto complete_syscall;

	got_empty_spot:

		/* Don't put */
		handle_table[empty_spot] = handle;

		new_handle = LOWEST_HANDLE + (empty_spot << 2U);
	}

	char bits[8U] = {0};
	memcpy(bits, &new_handle, sizeof bits);

	int result =
	    process_memcpy(process, TargetHandle, bits, sizeof bits);
	if (0 == status)
		status = errno_to_ntstatus(result);

complete_syscall:
	complete_syscall(process, status);
	return 0;
}

static int nt_close(struct process *process, uint64_t hFile)
{
	NTSTATUS error = 0;

	if (hFile < LOWEST_HANDLE) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	uint64_t ii = hFile - LOWEST_HANDLE;
	if ((ii & ((1U << UNUSED_BITS) - 1U)) != 0) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	ii = ii >> UNUSED_BITS;

	if (ii >= NUM_HANDLES) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	struct handle *real_handle = process->handle_table[ii];
	if (0 == real_handle) {
		error = _WUM_KERNEL_STATUS_INVALID_PARAMETER;
		goto complete_syscall;
	}

	process->handle_table[ii] = 0;

	handle_put(real_handle);

complete_syscall:
	complete_syscall(process, error);
	return 0;
}

static int proxy_mprotect(struct process *process, uint64_t addr,
                          size_t length, int prot)
{
	return proxy_syscall(process, __NR_mprotect, 0, addr, length,
	                     prot, 0, 0, 0);
}

static int proxy_munmap(struct process *process, uint64_t addr,
                        size_t length)
{
	return proxy_syscall(process, __NR_munmap, 0, addr, length, 0,
	                     0, 0, 0);
}

static int proxy_brk(struct process *process, uint64_t addr)
{
	return proxy_syscall(process, __NR_brk, 0, addr, 0, 0, 0, 0, 0);
}

static int proxy_write(struct process *process, uint64_t *resultp,
                       int fd, uint64_t buf, size_t count)
{
	return proxy_syscall(process, __NR_write, resultp, fd, buf,
	                     count, 0, 0, 0);
}

static int proxy_pread(struct process *process, uint64_t *resultp,
                       int fd, uint64_t buf, size_t count, off_t offset)
{
	return proxy_syscall(process, __NR_pread64, resultp, fd, buf,
	                     count, offset, 0, 0);
}

static int proxy_mmap(struct process *process, uint64_t *resultp,
                      uint64_t addr, size_t length, int prot, int flags,
                      int fd, off_t offset)
{
	return proxy_syscall(process, __NR_mmap, resultp, addr, length,
	                     prot, flags, fd, offset);
}

static uint64_t proxy_syscall(struct process *process, uint64_t sysno,
                              uint64_t *resultp, uint64_t arg1,
                              uint64_t arg2, uint64_t arg3,
                              uint64_t arg4, uint64_t arg5,
                              uint64_t arg6)
{
	int err = 0;
	struct user_regs_struct orig_registers = {0};

	err = ptrace_getregs(process->pid, &orig_registers);
	if (err != 0)
		return err;

	{
		struct user_regs_struct registers = {0};

		registers.cs = orig_registers.cs;
		registers.ss = orig_registers.ss;

		registers.rip = (uintptr_t)_WUM_KERNEL_ABI_SHARED_PAGESP
		                    ->execute_only.value.bytes;

		registers.orig_rax = sysno;
		registers.rax = sysno;
		registers.rdi = arg1;
		registers.rsi = arg2;
		registers.rdx = arg3;
		registers.r10 = arg4;
		registers.r8 = arg5;
		registers.r9 = arg6;

		err = ptrace_setregs(process->pid, &registers);
		if (err != 0)
			return err;
	}

	err = ptrace_syscall(process->pid);
	if (err != 0)
		return err;

	swapcontext(&process->syscall_context,
	            &process->normal_context);

	{
		struct user_regs_struct registers = orig_registers;

		registers.rip = (uintptr_t)_WUM_KERNEL_ABI_SHARED_PAGESP
		                    ->execute_only.value.bytes;

		registers.orig_rax = sysno;
		registers.rax = sysno;
		registers.rdi = arg1;
		registers.rsi = arg2;
		registers.rdx = arg3;
		registers.r10 = arg4;
		registers.r8 = arg5;
		registers.r9 = arg6;

		err = ptrace_setregs(process->pid, &registers);
		if (err != 0)
			return err;
	}

	err = ptrace_syscall(process->pid);
	if (err != 0)
		return err;

	swapcontext(&process->syscall_context,
	            &process->normal_context);

	int64_t result;
	{
		struct user_regs_struct registers;

		err = ptrace_getregs(process->pid, &registers);
		if (err != 0)
			return err;

		result = registers.rax;

		err = ptrace_setregs(process->pid, &orig_registers);
		if (err != 0)
			return err;
	}

	if (-4095 <= result && result < -1)
		return -result;

	if (resultp != 0)
		*resultp = result;
	return 0;
}

static NTSTATUS create_anonymous_memory_handle(struct handle **handlep,
                                               char const *name)
{
	NTSTATUS status = 0;
	int fd;
	struct handle *handle;
	char *name_dup;

	if (0 == name) {
		name_dup = 0;
	} else {
		name_dup = strdup(name);
		if (0 == name_dup)
			return errno_to_ntstatus(errno);
	}

	handle = malloc(sizeof *handle);
	if (0 == handle) {
		status = errno_to_ntstatus(errno);
		goto free_name;
	}

	fd = open("/dev/shm",
	          O_RDWR | O_NONBLOCK | O_TMPFILE | O_CLOEXEC, 0);
	if (-1 == fd) {
		status = errno_to_ntstatus(errno);
		goto free_handle;
	}

	/* Truncate to the size of an address space */
	if (-1 == ftruncate(fd, ADDRESS_SPACE_TOP)) {
		status = errno_to_ntstatus(errno);
		goto close_fd;
	}

	handle->fd = fd;
	handle->name = name_dup;
	*handlep = handle;
	return 0;

close_fd:
	close(fd);
free_handle:
	free(handle);
free_name:
	free(name_dup);
	return status;
}

static NTSTATUS create_fd_handle(struct handle **handlep, int fd,
                                 char const *name)
{
	NTSTATUS status = 0;
	struct handle *handle;
	char *name_dup;

	name_dup = strdup(name);
	if (0 == name_dup)
		return errno_to_ntstatus(errno);

	handle = malloc(sizeof *handle);
	if (0 == handle) {
		status = errno_to_ntstatus(errno);
		goto free_name;
	}

	handle->fd = fd;
	handle->refcount = 0U;
	handle->name = name_dup;
	*handlep = handle;
	return 0;

free_name:
	free(name_dup);
	return status;
}

static struct handle *process_handle_get(struct process *process,
                                         uint64_t handle)
{
	if (handle < LOWEST_HANDLE)
		return 0;

	uint64_t ii = handle - LOWEST_HANDLE;
	if ((ii & ((1U << UNUSED_BITS) - 1U)) != 0)
		return 0;

	ii = ii >> UNUSED_BITS;

	if (ii >= NUM_HANDLES)
		return 0;

	struct handle *real_handle = process->handle_table[ii];
	if (0 == real_handle)
		return 0;

	++real_handle->refcount;

	return real_handle;
}

static struct handle *handle_get(struct handle *handle)
{
	++handle->refcount;

	return handle;
}

static void handle_put(struct handle *handle)
{
	if (--handle->refcount != (unsigned)-1)
		return;

	int fd = handle->fd;

	close(fd);
}

static int process_memcpy(struct process *process, uint64_t dest,
                          void const *src, size_t size)
{
	ssize_t yy = pwrite(process->mem_fd, src, size, dest);
	if (-1 == yy)
		return errno;
	return 0;
}

static int insert_vma(struct process *process, struct vma *vma);

static int process_mmap(struct process *process, uint64_t *resultp,
                        uint64_t addr, uint64_t length, int prot,
                        int flags, struct handle *handle,
                        uint64_t offset)
{
	int err = 0;
	struct handle *anonymous_memory;
	struct vma *vma;

	bool has_map_anonymous;
	bool has_map_fixed;
	bool has_map_shared;

	bool readable;
	bool writable;
	bool executable;

	anonymous_memory = process->anonymous_memory;

	has_map_anonymous = (flags & MAP_ANONYMOUS) != 0U;
	has_map_fixed = (flags & MAP_FIXED) != 0U;
	has_map_shared = (flags & MAP_SHARED) != 0U;

	readable = (prot & PROT_READ) != 0U;
	writable = (prot & PROT_WRITE) != 0U;
	executable = (prot & PROT_EXEC) != 0U;

	if (has_map_anonymous) {
		if (handle != 0)
			return EINVAL;
		if (offset != 0U)
			return EINVAL;

		handle = anonymous_memory;
	}

	vma = malloc(sizeof *vma);
	if (0 == vma) {
		err = errno;
		assert(err != 0);
		return err;
	}

	if (!has_map_fixed) {
		uint64_t xx;
		err = proxy_mmap(process, &xx, addr, length, PROT_NONE,
		                 (flags & ~MAP_SHARED) | MAP_PRIVATE |
		                     MAP_ANONYMOUS,
		                 -1, 0);
		if (err != 0)
			goto free_vma;
		addr = xx;
	}

	if (has_map_anonymous)
		offset = addr;

	err = proxy_mmap(process, 0, addr, length, prot,
	                 (flags & ~MAP_PRIVATE & ~MAP_STACK &
	                  ~MAP_ANONYMOUS & ~MAP_GROWSDOWN) |
	                     MAP_FIXED | MAP_SHARED,
	                 handle->fd, offset);
	if (err != 0) {
		if (!has_map_fixed)
			proxy_munmap(process, addr, length);
		goto free_vma;
	}

	vma->stale = false;
	vma->handle = handle_get(handle);
	vma->offset = offset;
	vma->addr_bottom = addr;
	vma->addr_top = addr + length;
	vma->readable = readable;
	vma->writable = writable;
	vma->executable = executable;
	vma->shared = has_map_shared;

	err = insert_vma(process, vma);
	if (err != 0) {
		proxy_munmap(process, addr, length);
		goto free_vma;
	}

	process_gc_stale_vmas(process);

	if (resultp != 0)
		*resultp = addr;
	return 0;

free_vma:
	free(vma);
	return err;
}

static int insert_vma(struct process *process, struct vma *vma)
{
	struct vma_index lowest_vmap;
	struct vma_index lower_vmap;
	struct vma_index higher_vmap;

	struct handle *handle;
	uint64_t offset;
	uint64_t addr_bottom;
	uint64_t addr_top;

	bool readable;
	bool writable;
	bool executable;

	bool shared;

	bool merge_with_higher;
	bool merge_with_lower;

	lowest_vmap = get_lowest_vma(&process->vmas);

	handle = vma->handle;
	offset = vma->offset;
	addr_bottom = vma->addr_bottom;
	addr_top = vma->addr_top;
	readable = vma->readable;
	writable = vma->writable;
	executable = vma->executable;
	shared = vma->shared;

	merge_with_lower = false;
	merge_with_higher = false;

	bool found_lower_vma = false;
	{
		struct vma_index iip;
		lower_vmap = INVALID_VMA_INDEX;
		for (iip = lowest_vmap;
		     index_is_valid(&process->vmas, iip);
		     iip = get_next_vma(&process->vmas, iip)) {
			uint64_t ii_addr_bottom;
			struct vma *ii;

			ii = get_vma(&process->vmas, iip);

			ii_addr_bottom = ii->addr_bottom;

			{
				struct handle *ii_handle;
				uint64_t ii_offset;
				bool ii_readable;
				bool ii_writable;
				bool ii_executable;
				bool ii_shared;

				ii_handle = ii->handle;
				ii_offset = ii->offset;
				ii_readable = ii->readable;
				ii_writable = ii->writable;
				ii_executable = ii->executable;
				ii_shared = ii->shared;

				if (handle == ii_handle &&
				    addr_top == ii_addr_bottom &&
				    offset + addr_top - addr_bottom ==
				        ii_offset &&
				    readable == ii_readable &&
				    writable == ii_writable &&
				    executable == ii_executable &&
				    shared == ii_shared) {
					higher_vmap = iip;
					merge_with_higher = true;
				}
			}

			if (found_lower_vma) {
				struct vma *lower_vma;
				struct handle *lower_vma_handle;
				uint64_t lower_vma_addr_bottom;
				uint64_t lower_vma_addr_top;
				uint64_t lower_vma_offset;
				bool lower_vma_readable;
				bool lower_vma_writable;
				bool lower_vma_executable;
				bool lower_vma_shared;

				lower_vma =
				    get_vma(&process->vmas, lower_vmap);

				lower_vma_handle = lower_vma->handle;
				lower_vma_addr_bottom =
				    lower_vma->addr_bottom;
				lower_vma_addr_top =
				    lower_vma->addr_top;
				lower_vma_offset = lower_vma->offset;
				lower_vma_readable =
				    lower_vma->readable;
				lower_vma_writable =
				    lower_vma->writable;
				lower_vma_executable =
				    lower_vma->executable;
				lower_vma_shared = lower_vma->shared;

				if (handle == lower_vma_handle &&
				    addr_bottom == lower_vma_addr_top &&
				    offset ==
				        lower_vma_offset +
				            lower_vma_addr_top -
				            lower_vma_addr_bottom &&
				    readable == lower_vma_readable &&
				    writable == lower_vma_writable &&
				    executable ==
				        lower_vma_executable &&
				    shared == lower_vma_shared) {
					merge_with_lower = true;
				}
			}

			if (merge_with_lower || merge_with_higher)
				goto merge;

			if (addr_top <= ii_addr_bottom) {
				higher_vmap = iip;
				goto found_vma;
			}
			found_lower_vma = true;
			lower_vmap = iip;
		}
	}

	higher_vmap = INVALID_VMA_INDEX;

found_vma:
	;
	struct vma_node *node = malloc(sizeof *node);
	assert(node != 0);
	node->_vma = vma;
	node->_higher_node = higher_vmap._node;

	if (found_lower_vma) {
		lower_vmap._node->_higher_node = node;
	} else {
		process->vmas._lowest_node = node;
	}

	return 0;

merge:
	process_mark_vma_as_stale(process, vma);

	if (merge_with_higher && merge_with_lower) {
		struct vma_node *lower_node = lower_vmap._node;
		struct vma_node *higher_node = higher_vmap._node;

		lower_node->_vma->addr_top =
		    higher_node->_vma->addr_top;
		lower_node->_higher_node = higher_node->_higher_node;

		process_mark_vma_as_stale(process, higher_node->_vma);
		return 0;
	}

	if (merge_with_higher) {
		struct vma *higher_vma =
		    get_vma(&process->vmas, higher_vmap);
		higher_vma->addr_bottom = addr_bottom;
		higher_vma->addr_top = addr_top;
		return 0;
	}

	if (merge_with_lower) {
		get_vma(&process->vmas, lower_vmap)->addr_top =
		    addr_top;
		return 0;
	}

	return 0;
}

static void process_mark_vma_as_stale(struct process *process,
                                      struct vma *vma)
{
	struct vma_node *stale_vmas;

	stale_vmas = process->stale_vmas._lowest_node;

	vma->stale = true;

	struct vma_node *new_node = malloc(sizeof *vma);
	assert(new_node != 0);

	new_node->_vma = vma;
	new_node->_higher_node = stale_vmas;

	if (0 == stale_vmas) {
		process->stale_vmas._lowest_node = new_node;
	} else {
		stale_vmas->_higher_node->_vma = vma;
	}
}

static void process_gc_stale_vmas(struct process *process)
{
	struct vma_node *stale_vmas;
	struct vma_node *ii;

	stale_vmas = process->stale_vmas._lowest_node;

	for (ii = stale_vmas; ii != 0;) {
		struct vma_node *next_node;
		struct handle *handle;

		handle = ii->_vma->handle;
		next_node = ii->_higher_node;

		handle_put(handle);

		free(ii->_vma);
		free(ii);

		ii = next_node;
	}

	process->stale_vmas._lowest_node = 0;
}

static int process_munmap(struct process *process, uint64_t addr,
                          uint64_t length)
{
	int err = 0;
	struct vma_index lowest_vmap;
	struct vma_index lower_vmap;
	struct vma_index vma_to_be_unmappedp;
	struct vma_index higher_vmap;

	lowest_vmap = get_lowest_vma(&process->vmas);

	bool found_lower_vma = false;
	{
		struct vma_index iip;
		lower_vmap = INVALID_VMA_INDEX;
		for (iip = lowest_vmap;
		     index_is_valid(&process->vmas, iip);
		     iip = get_next_vma(&process->vmas, iip)) {
			struct vma *ii;
			uint64_t ii_addr_bottom;
			uint64_t ii_addr_top;

			ii = get_vma(&process->vmas, iip);

			ii_addr_bottom = ii->addr_bottom;
			ii_addr_top = ii->addr_top;

			/* TODO: Handle partial unmaps */
			if (ii_addr_bottom == addr &&
			    ii_addr_top == addr + length) {
				vma_to_be_unmappedp = iip;
				goto found_vma;
			}
			found_lower_vma = true;
			lower_vmap = iip;
		}
	}

	/* The region is already unmapped */
	return 0;

found_vma:
	err = proxy_munmap(process, addr, length);
	if (err != 0)
		return err;

	higher_vmap = get_next_vma(&process->vmas, vma_to_be_unmappedp);

	if (found_lower_vma) {
		lower_vmap._node->_higher_node = higher_vmap._node;
	} else {
		process->vmas._lowest_node = higher_vmap._node;
	}

	struct vma *vma_to_be_unmapped =
	    get_vma(&process->vmas, vma_to_be_unmappedp);
	handle_put(vma_to_be_unmapped->handle);

	free(vma_to_be_unmapped);

	return 0;
}

static int process_mprotect(struct process *process, uint64_t addr,
                            size_t length, int prot)
{
	int err = 0;
	struct vma_index lowest_vmap;
	bool readable;
	bool writable;
	bool executable;
	size_t num_vmas_to_be_changed;
	struct vma_index *vmas_to_be_changed;
	uint64_t addr_top;

	readable = (prot & PROT_READ) != 0U;
	writable = (prot & PROT_WRITE) != 0U;
	executable = (prot & PROT_EXEC) != 0U;

	lowest_vmap = get_lowest_vma(&process->vmas);

	addr_top = addr + length;

	num_vmas_to_be_changed = 0U;
	vmas_to_be_changed = 0;
	{
		struct vma_index iip;
		for (iip = lowest_vmap;
		     index_is_valid(&process->vmas, iip);
		     iip = get_next_vma(&process->vmas, iip)) {
			uint64_t ii_addr_bottom;
			uint64_t ii_addr_top;
			struct vma *ii;

			ii = get_vma(&process->vmas, iip);

			ii_addr_bottom = ii->addr_bottom;
			ii_addr_top = ii->addr_top;

			if (addr_top <= ii_addr_bottom)
				continue;

			if (addr >= ii_addr_top)
				continue;

			struct vma_index *xx =
			    realloc(vmas_to_be_changed,
			            (num_vmas_to_be_changed + 1U) *
			                sizeof vmas_to_be_changed[0U]);
			if (0 == xx) {
				err = errno;
				goto free_vmas_to_be_changed;
			}
			vmas_to_be_changed = xx;

			vmas_to_be_changed[num_vmas_to_be_changed] =
			    iip;

			++num_vmas_to_be_changed;
		}
	}
	if (0U == num_vmas_to_be_changed) {
		err = ENOMEM;
		goto free_vmas_to_be_changed;
	}

	{
		size_t ii;
		uint64_t lowest_point;
		struct vma_index vmap;
		struct vma *vma;

		lowest_point = addr;
		for (ii = 0U; ii < num_vmas_to_be_changed; ++ii) {
			uint64_t vma_addr_bottom;
			uint64_t vma_addr_top;

			vmap = vmas_to_be_changed[ii];
			vma = get_vma(&process->vmas, vmap);

			vma_addr_bottom = vma->addr_bottom;
			vma_addr_top = vma->addr_top;

			if (lowest_point < vma_addr_bottom) {
				err = ENOMEM;
				goto free_vmas_to_be_changed;
			}

			lowest_point = vma_addr_top;
		}

		if (addr_top > vma->addr_top) {
			err = ENOMEM;
			goto free_vmas_to_be_changed;
		}
	}

	{
		size_t ii;
		struct vma_index vmap;
		struct vma *vma;

		for (ii = 0U; ii < num_vmas_to_be_changed; ++ii) {
			uint64_t vma_offset;
			uint64_t vma_addr_bottom;
			uint64_t vma_addr_top;
			bool higher;
			bool lower;

			vmap = vmas_to_be_changed[ii];
			vma = get_vma(&process->vmas, vmap);

			if (vma->stale)
				continue;

			vma_offset = vma->offset;
			vma_addr_bottom = vma->addr_bottom;
			vma_addr_top = vma->addr_top;

			higher = vma_addr_bottom < addr;
			lower = addr_top < vma_addr_top;

			if (higher && lower) {
				struct vma *middle_vma;
				struct vma *highest_vma;

				middle_vma = malloc(sizeof *middle_vma);
				if (0 == middle_vma) {
					err = errno;
					assert(err != 0);
					goto free_vmas_to_be_changed;
				}
				highest_vma =
				    malloc(sizeof *highest_vma);
				if (0 == highest_vma) {
					err = errno;
					assert(err != 0);
					free(middle_vma);
					goto free_vmas_to_be_changed;
				}

				middle_vma->stale = false;
				middle_vma->handle =
				    handle_get(vma->handle);
				middle_vma->offset =
				    vma_offset + addr - vma_addr_bottom;
				middle_vma->addr_bottom = addr;
				middle_vma->addr_top = addr_top;
				middle_vma->readable = readable;
				middle_vma->writable = writable;
				middle_vma->executable = executable;
				middle_vma->shared = vma->shared;

				highest_vma->stale = false;
				highest_vma->handle =
				    handle_get(vma->handle);
				highest_vma->offset = vma_offset +
				                      addr_top -
				                      vma_addr_bottom;
				highest_vma->addr_bottom = addr_top;
				highest_vma->addr_top = vma_addr_top;
				highest_vma->readable = vma->readable;
				highest_vma->writable = vma->writable;
				highest_vma->executable =
				    vma->executable;
				highest_vma->shared = vma->shared;

				vma->addr_top = addr;

				err = insert_vma(process, middle_vma);
				err = insert_vma(process, highest_vma);
				continue;
			}

			if (higher) {
				struct vma *new_vma;

				new_vma = malloc(sizeof *new_vma);
				if (0 == new_vma) {
					err = errno;
					assert(err != 0);
					goto free_vmas_to_be_changed;
				}

				new_vma->stale = false;
				new_vma->handle =
				    handle_get(vma->handle);
				new_vma->offset =
				    vma_offset + addr - vma_addr_bottom;
				new_vma->addr_bottom = addr;
				new_vma->addr_top = vma_addr_top;
				new_vma->readable = readable;
				new_vma->writable = writable;
				new_vma->executable = executable;
				new_vma->shared = vma->shared;

				vma->addr_top = addr;

				err = insert_vma(process, new_vma);
				continue;
			}

			if (lower) {
				struct vma *new_vma;

				new_vma = malloc(sizeof *new_vma);
				if (0 == new_vma) {
					err = errno;
					assert(err != 0);
					goto free_vmas_to_be_changed;
				}

				new_vma->stale = false;
				new_vma->handle =
				    handle_get(vma->handle);
				new_vma->offset = vma_offset;
				new_vma->addr_bottom = vma_addr_bottom;
				new_vma->addr_top = addr_top;
				new_vma->readable = readable;
				new_vma->writable = writable;
				new_vma->executable = executable;
				new_vma->shared = vma->shared;

				vma->offset = vma_offset + addr_top -
				              vma_addr_bottom;
				vma->addr_bottom = addr_top;

				err = insert_vma(process, new_vma);
				continue;
			}

			vma->readable = readable;
			vma->writable = writable;
			vma->executable = executable;
		}
	}

	err = proxy_mprotect(process, addr, length, prot);
	if (err != 0)
		goto free_vmas_to_be_changed;

free_vmas_to_be_changed:
	free(vmas_to_be_changed);

	process_gc_stale_vmas(process);

	return err;
}

/* TODO: Implement errno_to_ntstatus more fully */
static NTSTATUS errno_to_ntstatus(int err)
{
	switch (err) {
	case 0:
		return _WUM_KERNEL_STATUS_SUCCESS;

	case EINVAL:
		return _WUM_KERNEL_STATUS_INVALID_PARAMETER;

	case ENOSYS:
		return _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;

	case EPERM:
		return _WUM_KERNEL_STATUS_INVALID_HANDLE;

	case ENOENT:
		return _WUM_KERNEL_STATUS_NO_SUCH_FILE;

	case EIO:
		return _WUM_KERNEL_STATUS_UNEXPECTED_IO_ERROR;

	case ENXIO:
		return _WUM_KERNEL_STATUS_DEVICE_REMOVED;

	case ENOEXEC:
		return _WUM_KERNEL_STATUS_INVALID_IMAGE_FORMAT;

	case ENOMEM:
		return _WUM_KERNEL_STATUS_NO_MEMORY;

	case EBUSY:
		return _WUM_KERNEL_STATUS_DEVICE_BUSY;

	case ENODEV:
		return _WUM_KERNEL_STATUS_INVALID_DEVICE_REQUEST;

	case ENOTDIR:
		return _WUM_KERNEL_STATUS_NOT_A_DIRECTORY;

	case EISDIR:
		return _WUM_KERNEL_STATUS_FILE_IS_A_DIRECTORY;

	case ENFILE:
	case EMFILE:
		return _WUM_KERNEL_STATUS_TOO_MANY_OPENED_FILES;

	case EPIPE:
		return _WUM_KERNEL_STATUS_PIPE_BROKEN;

	case ENOTEMPTY:
		return _WUM_KERNEL_STATUS_DIRECTORY_NOT_EMPTY;

	case EINTR:
	case EBADF:
	case EXDEV:
	case EDOM:
	case ERANGE:
		/* These should all be catched before they reach
		 * user-space */
		abort();

	case ESRCH:
	case E2BIG:
	case ECHILD:
	case EACCES:
	case EFAULT:
	case ENOTBLK:
	case EEXIST:
	case ENOTTY:
	case ETXTBSY:
	case EFBIG:
	case ENOSPC:
	case ESPIPE:
	case EROFS:
	case EMLINK:

	case EDEADLK:
#if EDEADLK != EDEADLOCK
	case EDEADLOCK:
#endif

	case ENAMETOOLONG:
	case ENOLCK:
	case ELOOP:

	case EAGAIN:
#if EWOULDBLOCK != EAGAIN
	case EWOULDBLOCK:
#endif

	case ENOMSG:
	case EIDRM:
	case ECHRNG:
	case EL2NSYNC:
	case EL3HLT:
	case EL3RST:
	case ELNRNG:
	case EUNATCH:
	case ENOCSI:
	case EL2HLT:
	case EBADE:
	case EBADR:
	case EXFULL:
	case ENOANO:
	case EBADRQC:
	case EBADSLT:
	case EBFONT:
	case ENOSTR:
	case ENODATA:
	case ETIME:
	case ENOSR:
	case ENONET:
	case ENOPKG:
	case EREMOTE:
	case ENOLINK:
	case EADV:
	case ESRMNT:
	case ECOMM:
	case EPROTO:
	case EMULTIHOP:
	case EDOTDOT:
	case EBADMSG:
	case EOVERFLOW:
	case ENOTUNIQ:
	case EBADFD:
	case EREMCHG:
	case ELIBACC:
	case ELIBBAD:
	case ELIBSCN:
	case ELIBMAX:
	case ELIBEXEC:
	case EILSEQ:
	case ERESTART:
	case ESTRPIPE:
	case EUSERS:
	case ENOTSOCK:
	case EDESTADDRREQ:
	case EMSGSIZE:
	case EPROTOTYPE:
	case ENOPROTOOPT:
	case EPROTONOSUPPORT:
	case ESOCKTNOSUPPORT:
	case EOPNOTSUPP:
	case EPFNOSUPPORT:
	case EAFNOSUPPORT:
	case EADDRINUSE:
	case EADDRNOTAVAIL:
	case ENETDOWN:
	case ENETUNREACH:
	case ENETRESET:
	case ECONNABORTED:
	case ECONNRESET:
	case ENOBUFS:
	case EISCONN:
	case ENOTCONN:
	case ESHUTDOWN:
	case ETOOMANYREFS:
	case ETIMEDOUT:
	case ECONNREFUSED:
	case EHOSTDOWN:
	case EHOSTUNREACH:
	case EALREADY:
	case EINPROGRESS:
	case ESTALE:
	case EUCLEAN:
	case ENOTNAM:
	case ENAVAIL:
	case EISNAM:
	case EREMOTEIO:
	case EDQUOT:
	case ENOMEDIUM:
	case EMEDIUMTYPE:
	case ECANCELED:
	case ENOKEY:
	case EKEYEXPIRED:
	case EKEYREVOKED:
	case EKEYREJECTED:
	case EOWNERDEAD:
	case ENOTRECOVERABLE:
	case ERFKILL:
	case EHWPOISON:

	default:
		return _WUM_KERNEL_STATUS_NOT_IMPLEMENTED;
	}
}

static char const *ntstatus_str(NTSTATUS status)
{
	switch (status) {
	case _WUM_KERNEL_STATUS_SUCCESS:
		return "Success";

	case _WUM_KERNEL_STATUS_INVALID_PARAMETER:
		return "Invalid Parameter";

	case _WUM_KERNEL_STATUS_NOT_IMPLEMENTED:
		return "Not Impelemented";

	default:
		return "Unknown";
	}
}
