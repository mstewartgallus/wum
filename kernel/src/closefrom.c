/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define _POSIX_C_SOURCE 200809L

#include "config.h"

#include "_wum-kernel/closefrom.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Abort on failure because exit handlers and other code might not
 * react securely to leaked file descriptors.
 */
void wum_kernel_closefrom(int lowfd)
{
	int err;
	int fds_dir_fd;
	DIR *fds_dir;

	fds_dir_fd = open("/proc/self/fd", O_CLOEXEC | O_DIRECTORY);
	if (-1 == fds_dir_fd)
		abort();

	fds_dir = fdopendir(fds_dir_fd);
	if (0 == fds_dir)
		abort();

	int *fds_to_close = 0;
	size_t num_fds_to_close = 0U;
	/* Read all the open fds first and then close the fds after
	 * because otherwise there is a race condition */
	for (;;) {
		errno = 0;
		struct dirent *direntry = readdir(fds_dir);
		if (0 == direntry) {
			err = errno;
			if (err != 0)
				abort();
			break;
		}

		char const *fdname = direntry->d_name;

		if (0 == strcmp(".", fdname))
			continue;
		if (0 == strcmp("..", fdname))
			continue;

		int open_fd = atoi(fdname);
		if (open_fd < lowfd)
			continue;

		if (dirfd(fds_dir) == open_fd)
			continue;

		{
			void *xx = realloc(fds_to_close,
			                   (num_fds_to_close + 1U) *
			                       sizeof fds_to_close[0U]);
			if (0 == xx)
				abort();
			fds_to_close = (int *)xx;
		}
		fds_to_close[num_fds_to_close] = open_fd;
		++num_fds_to_close;
	}

	if (-1 == closedir(fds_dir)) {
		err = errno;
	}
	if (err != 0)
		abort();

	/* Deliberately don't check the closed fds as they could be
	 * used by attackers deliberately.
	 */
	{
		size_t ii;
		for (ii = 0U; ii < num_fds_to_close; ++ii)
			close(fds_to_close[ii]);
	}
}
