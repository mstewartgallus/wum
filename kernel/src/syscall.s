.section .text, "ax", @progbits
.globl _wum_kernel_syscall_instr_start
.hidden _wum_kernel_syscall_instr_start
.type _wum_kernel_syscall_instr_start, @function
_wum_kernel_syscall_instr_start:
	syscall
.size _wum_kernel_syscall_instr_start, .-_wum_kernel_syscall_instr_start
.globl _wum_kernel_syscall_instr_end
.hidden _wum_kernel_syscall_instr_end
_wum_kernel_syscall_instr_end: