/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef __WUM_KERNEL_PTRACE_H
#define __WUM_KERNEL_PTRACE_H 1

#include <errno.h>
#include <inttypes.h>
#include <signal.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>

static int ptrace_setfpregs(pid_t pid,
                            struct user_fpregs_struct const *regs);
static int ptrace_setregs(pid_t pid,
                          struct user_regs_struct const *regs);

static int ptrace_getfpregs(pid_t pid, struct user_fpregs_struct *regs);
static int ptrace_getregs(pid_t pid, struct user_regs_struct *regs);

static int ptrace_getsiginfo(pid_t pid, siginfo_t *infop);

static int ptrace_interrupt(pid_t pid);
static int ptrace_seize(pid_t pid, unsigned options);
static int ptrace_syscall(pid_t pid);
static int ptrace_cont(pid_t pid, int signo);

static int ptrace_setfpregs(pid_t pid,
                            struct user_fpregs_struct const *regs)
{
	if (-1 == ptrace(PTRACE_SETFPREGS, (pid_t)pid, (void *)0,
	                 (void const *)regs)) {
		return errno;
	}
	return 0;
}

static int ptrace_setregs(pid_t pid,
                          struct user_regs_struct const *regs)
{
	if (-1 == ptrace(PTRACE_SETREGS, (pid_t)pid, (void *)0,
	                 (void const *)regs)) {
		return errno;
	}
	return 0;
}

static int ptrace_getregs(pid_t pid, struct user_regs_struct *regs)
{
	if (-1 == ptrace(PTRACE_GETREGS, (pid_t)pid, (void *)0,
	                 (void *)regs)) {
		return errno;
	}
	return 0;
}

static int ptrace_getfpregs(pid_t pid, struct user_fpregs_struct *regs)
{
	if (-1 == ptrace(PTRACE_GETFPREGS, (pid_t)pid, (void *)0,
	                 (void *)regs)) {
		return errno;
	}
	return 0;
}

static int ptrace_getsiginfo(pid_t pid, siginfo_t *infop)
{
	if (-1 == ptrace(PTRACE_GETSIGINFO, (pid_t)pid, (void *)0,
	                 (void *)infop)) {
		return errno;
	}
	return 0;
}

static int ptrace_seize(pid_t pid, unsigned options)
{
	if (-1 == ptrace(PTRACE_SEIZE, (pid_t)pid, (void *)0,
	                 (void *)(uintptr_t)options)) {
		return errno;
	}
	return 0;
}

static int ptrace_interrupt(pid_t pid)
{
	if (-1 == ptrace(PTRACE_INTERRUPT, (pid_t)pid, (void *)0,
	                 (void *)0)) {
		return errno;
	}
	return 0;
}

static int ptrace_syscall(pid_t pid)
{
	if (-1 ==
	    ptrace(PTRACE_SYSCALL, (pid_t)pid, (void *)0, (void *)0)) {
		return errno;
	}
	return 0;
}

static int ptrace_cont(pid_t pid, int signo)
{
	if (-1 == ptrace(PTRACE_CONT, (pid_t)pid, (void *)0,
	                 (void *)(intptr_t)signo)) {
		return errno;
	}
	return 0;
}

#endif
