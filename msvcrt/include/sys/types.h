/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_SYS_TYPES_H
#define _WUM_MSVCRT_SYS_TYPES_H 1

#include "../internal/sys/types.h"

typedef __wum_msvcrt_ssize_t ssize_t;
typedef __wum_msvcrt_socklen_t socklen_t;
typedef __wum_msvcrt_pid_t pid_t;

typedef long int time_t;
typedef long int suseconds_t;

#endif
