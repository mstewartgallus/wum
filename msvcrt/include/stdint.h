/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_STDINT_H
#define _WUM_MSVCRT_STDINT_H 1

#include <wum-kernel/stdint.h>

#define NULL 0

typedef __wum_kernel_intptr_t intptr_t;
typedef __wum_kernel_uintptr_t uintptr_t;

typedef __wum_kernel_int32_t int32_t;
typedef __wum_kernel_uint32_t uint32_t;

typedef __wum_kernel_int64_t int64_t;
typedef __wum_kernel_uint64_t uint64_t;

#define UINTPTR_C(X) X##UL

#endif
