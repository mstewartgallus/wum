/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_ERRNO_H
#define _WUM_MSVCRT_ERRNO_H 1

#include "internal/api.h"
#include "internal/errno.h"

#define ELOOP ((int)_WUM_MSVCRT_ELOOP)
#define EPERM ((int)_WUM_MSVCRT_EPERM)
#define ENOTCONN ((int)_WUM_MSVCRT_ENOTCONN)
#define E2BIG ((int)_WUM_MSVCRT_E2BIG)
#define EDOM ((int)_WUM_MSVCRT_EDOM)
#define ECONNREFUSED ((int)_WUM_MSVCRT_ECONNREFUSED)
#define EDQUOT ((int)_WUM_MSVCRT_EDQUOT)
#define EBADMSG ((int)_WUM_MSVCRT_EBADMSG)
#define EMLINK ((int)_WUM_MSVCRT_EMLINK)
#define EBUSY ((int)_WUM_MSVCRT_EBUSY)
#define ENOLCK ((int)_WUM_MSVCRT_ENOLCK)
#define EOVERFLOW ((int)_WUM_MSVCRT_EOVERFLOW)
#define EINVAL ((int)_WUM_MSVCRT_EINVAL)
#define EFBIG ((int)_WUM_MSVCRT_EFBIG)
#define ESPIPE ((int)_WUM_MSVCRT_ESPIPE)
#define ENOMSG ((int)_WUM_MSVCRT_ENOMSG)
#define EMSGSIZE ((int)_WUM_MSVCRT_EMSGSIZE)
#define EEXIST ((int)_WUM_MSVCRT_EEXIST)
#define EWOULDBLOCK EAGAIN
#define EMFILE ((int)_WUM_MSVCRT_EMFILE)
#define ECONNRESET ((int)_WUM_MSVCRT_ECONNRESET)
#define ENETDOWN ((int)_WUM_MSVCRT_ENETDOWN)
#define ENOTTY ((int)_WUM_MSVCRT_ENOTTY)
#define ENFILE ((int)_WUM_MSVCRT_ENFILE)
#define EILSEQ ((int)_WUM_MSVCRT_EILSEQ)
#define ENXIO ((int)_WUM_MSVCRT_ENXIO)
#define EDEADLK ((int)_WUM_MSVCRT_EDEADLK)
#define EISDIR ((int)_WUM_MSVCRT_EISDIR)
#define EINPROGRESS ((int)_WUM_MSVCRT_EINPROGRESS)
#define EISCONN ((int)_WUM_MSVCRT_EISCONN)
#define ENOMEM ((int)_WUM_MSVCRT_ENOMEM)
#define ENETUNREACH ((int)_WUM_MSVCRT_ENETUNREACH)
#define EOPNOTSUPP ((int)_WUM_MSVCRT_EOPNOTSUPP)
#define EDESTADDRREQ ((int)_WUM_MSVCRT_EDESTADDRREQ)
#define EPROTOTYPE ((int)_WUM_MSVCRT_EPROTOTYPE)
#define EROFS ((int)_WUM_MSVCRT_EROFS)
#define EALREADY ((int)_WUM_MSVCRT_EALREADY)
#define EOWNERDEAD ((int)_WUM_MSVCRT_EOWNERDEAD)
#define ENOLINK ((int)_WUM_MSVCRT_ENOLINK)
#define ECANCELED ((int)_WUM_MSVCRT_ECANCELED)
#define ESTALE ((int)_WUM_MSVCRT_ESTALE)
#define EINTR ((int)_WUM_MSVCRT_EINTR)
#define ENOENT ((int)_WUM_MSVCRT_ENOENT)
#define ECHILD ((int)_WUM_MSVCRT_ECHILD)
#define ESRCH ((int)_WUM_MSVCRT_ESRCH)
#define ENODATA ((int)_WUM_MSVCRT_ENODATA)
#define EPIPE ((int)_WUM_MSVCRT_EPIPE)
#define ENOEXEC ((int)_WUM_MSVCRT_ENOEXEC)
#define ENOTSUP EOPNOTSUPP
#define ENOSYS ((int)_WUM_MSVCRT_ENOSYS)
#define ENOTRECOVERABLE ((int)_WUM_MSVCRT_ENOTRECOVERABLE)
#define EIDRM ((int)_WUM_MSVCRT_EIDRM)
#define ENAMETOOLONG ((int)_WUM_MSVCRT_ENAMETOOLONG)
#define EAGAIN ((int)_WUM_MSVCRT_EAGAIN)
#define ENETRESET ((int)_WUM_MSVCRT_ENETRESET)
#define ENOBUFS ((int)_WUM_MSVCRT_ENOBUFS)
#define EPROTONOSUPPORT ((int)_WUM_MSVCRT_EPROTONOSUPPORT)
#define ENOTEMPTY ((int)_WUM_MSVCRT_ENOTEMPTY)
#define EIO ((int)_WUM_MSVCRT_EIO)
#define EADDRINUSE ((int)_WUM_MSVCRT_EADDRINUSE)
#define EFAULT ((int)_WUM_MSVCRT_EFAULT)
#define EAFNOSUPPORT ((int)_WUM_MSVCRT_EAFNOSUPPORT)
#define ECONNABORTED ((int)_WUM_MSVCRT_ECONNABORTED)
#define EADDRNOTAVAIL ((int)_WUM_MSVCRT_EADDRNOTAVAIL)
#define ENOSPC ((int)_WUM_MSVCRT_ENOSPC)
#define EHOSTUNREACH ((int)_WUM_MSVCRT_EHOSTUNREACH)
#define EMULTIHOP ((int)_WUM_MSVCRT_EMULTIHOP)
#define EBADF ((int)_WUM_MSVCRT_EBADF)
#define EXDEV ((int)_WUM_MSVCRT_EXDEV)
#define EPROTO ((int)_WUM_MSVCRT_EPROTO)
#define ENOPROTOOPT ((int)_WUM_MSVCRT_ENOPROTOOPT)
#define ENOTSOCK ((int)_WUM_MSVCRT_ENOTSOCK)
#define ENODEV ((int)_WUM_MSVCRT_ENODEV)
#define ETXTBSY ((int)_WUM_MSVCRT_ETXTBSY)
#define ETIMEDOUT ((int)_WUM_MSVCRT_ETIMEDOUT)
#define ENOTDIR ((int)_WUM_MSVCRT_ENOTDIR)
#define ERANGE ((int)_WUM_MSVCRT_ERANGE)
#define EACCES ((int)_WUM_MSVCRT_EACCES)

typedef __wum_msvcrt_errno_t errno_t;

_WUM_MSVCRT_INTERNAL_API volatile errno_t *
__wum_msvcrt_errno(void) _WUM_MSVCRT_INTERNAL_API_END;

#define errno (*__wum_msvcrt_errno())

#endif
