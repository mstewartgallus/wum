/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#ifndef _WUM_MSVCRT_STDIO_H
#define _WUM_MSVCRT_STDIO_H 1

#include "internal/api.h"

#include <wum-kernel/stddef.h>

#define EOF (-1)

#define stdin __wum_msvcrt_stdin()
#define stdout __wum_msvcrt_stdout()
#define stderr __wum_msvcrt_stderr()

struct __wum_msvcrt_FILE;
typedef struct __wum_msvcrt_FILE FILE;

_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stdin(void) _WUM_MSVCRT_INTERNAL_API_END;
_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stderr(void) _WUM_MSVCRT_INTERNAL_API_END;
_WUM_MSVCRT_INTERNAL_API struct __wum_msvcrt_FILE *
__wum_msvcrt_stdout(void) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API void
flockfile(struct __wum_msvcrt_FILE *__filehandle)
    _WUM_MSVCRT_INTERNAL_API_END;
_WUM_MSVCRT_INTERNAL_API int
ftrylockfile(struct __wum_msvcrt_FILE *__filehandle)
    _WUM_MSVCRT_INTERNAL_API_END;
_WUM_MSVCRT_INTERNAL_API void
funlockfile(struct __wum_msvcrt_FILE *__filehandle)
    _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API int
fputs(char const *__s, FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API int
fputc(int __c, FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API __wum_kernel_size_t
fwrite(void const *__ptr, __wum_kernel_size_t __size,
       __wum_kernel_size_t __nmemb,
       FILE *__stream) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API int
puts(char const *__s) _WUM_MSVCRT_INTERNAL_API_END;

_WUM_MSVCRT_INTERNAL_API void
perror(char const *__s) _WUM_MSVCRT_INTERNAL_API_END;

#endif
