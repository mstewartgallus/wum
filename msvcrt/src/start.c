/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include "__msvcrt/unistd.h"

#include <wchar.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <winbase.h>

extern int main(int argc, char **argv) __attribute__((weak));
extern int wmain(int wargc, wchar_t **wargv) __attribute__((weak));
extern int WinMain(HANDLE hInstance, HANDLE hInstancePrev,
                   char *lpszCmdLine, int nCmdShow)
    __attribute__((weak));
extern int wWinMain(HANDLE hInstance, HANDLE hInstancePrev,
                    wchar_t *lpszCmdLine, int nCmdShow)
    __attribute__((weak));

static void initialize_crt(void);

static char const *const arg[] = {"binary", 0};
static wchar_t const *const larg[] = {L"binary", 0};

extern void mainCRTStartup(void)
{
	initialize_crt();
	int exit_status = main(1, (char **)arg);
	exit(exit_status);
}

extern void mainACRTStartup(void)
{
	initialize_crt();
	int exit_status = main(1, (char **)arg);
	exit(exit_status);
}

extern void wmainCRTStartup(void)
{
	initialize_crt();
	int exit_status = wmain(1, (wchar_t **)larg);
	exit(exit_status);
}

extern void mainWCRTStartup(void)
{
	initialize_crt();
	int exit_status = wmain(1, (wchar_t **)larg);
	exit(exit_status);
}

extern void winMainCRTStartup(void)
{
	initialize_crt();
	int exit_status = WinMain(0, 0, (char *)"binary", 0);
	exit(exit_status);
}

extern void wWinMainCRTStartup(void)
{
	initialize_crt();
	int exit_status = wWinMain(0, 0, (wchar_t *)L"binary", 0);
	exit(exit_status);
}

extern void __main(void)
{ /* Empty */
}

static void initialize_crt(void)
{
	_wum_msvcrt_unistd_init();
}
