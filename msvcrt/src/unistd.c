/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include "__msvcrt/errno.h"

#include "ntdll/nt.h"

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <handleapi.h>
#include <processenv.h>
#include <ntstatus.h>
#include <winbase.h>

static int ntstatus_to_errno(__wum_kernel_NTSTATUS err);

static __wum_kernel_HANDLE lookup_fd(int fd);

_WUM_MSVCRT_INTERNAL_API void exit(int arg) _WUM_MSVCRT_INTERNAL_API_END
{
	NtTerminateProcess(GetCurrentProcess(), arg);

	*((char volatile *)0) = 0xFF;

	for (;;) {
		char volatile xx;
		xx = 0x00U;
	}
}

_WUM_MSVCRT_INTERNAL_API __wum_msvcrt_ssize_t
write(int fd, void const *buf,
      __wum_kernel_size_t n) _WUM_MSVCRT_INTERNAL_API_END
{
	__wum_kernel_HANDLE hFile = lookup_fd(fd);
	if (INVALID_HANDLE_VALUE == hFile)
		return EINVAL;

	IO_STATUS_BLOCK io_status;
	__wum_kernel_NTSTATUS status =
	    NtWriteFile(hFile, 0, 0, 0, &io_status, buf, n, 0, 0);
	if (status != 0) {
		_wum_msvcrt_set_errno(ntstatus_to_errno(status));
		return -1;
	}
	return 0;
}

static __wum_kernel_HANDLE fd_table[2048U] = {INVALID_HANDLE_VALUE};

void _wum_msvcrt_unistd_init(void)
{
	fd_table[0U] = GetStdHandle(STD_INPUT_HANDLE);
	fd_table[1U] = GetStdHandle(STD_OUTPUT_HANDLE);
	fd_table[2U] = GetStdHandle(STD_ERROR_HANDLE);
}

static __wum_kernel_HANDLE lookup_fd(int fd)
{
	if (fd < 0)
		return INVALID_HANDLE_VALUE;

	if (fd >= 2048)
		return INVALID_HANDLE_VALUE;

	return fd_table[fd];
}

/* TODO: Implement ntstatus_to_errno more fully */
static int ntstatus_to_errno(__wum_kernel_NTSTATUS err)
{
	switch (err) {
	case _WUM_KERNEL_STATUS_SUCCESS:
		return 0;

	case _WUM_KERNEL_STATUS_INVALID_PARAMETER:
		return EINVAL;

	case _WUM_KERNEL_STATUS_NOT_IMPLEMENTED:
		return ENOSYS;

	default:
		return ENOSYS;
	}
}
