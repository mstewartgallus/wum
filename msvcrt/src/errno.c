/*
 * Copyright 2015 Steven Stewart-Gallus
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */
#define STRICT

#include "config.h"

#include "__msvcrt/errno.h"

#include <errno.h>

static volatile errno_t _my_errno;

_WUM_MSVCRT_INTERNAL_API volatile errno_t *
__wum_msvcrt_errno(void) _WUM_MSVCRT_INTERNAL_API_END
{
	return &_my_errno;
}

void _wum_msvcrt_set_errno(errno_t new_err)
{
	_my_errno = new_err;
}
