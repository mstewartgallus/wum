dnl -*-Autoconf-*-
dnl This file is free software; as a special exception the author gives
dnl unlimited permission to copy and/or distribute it, with or without
dnl modifications, as long as this notice is preserved.
dnl
dnl This program is distributed in the hope that it will be useful, but
dnl WITHOUT ANY WARRANTY, to the extent permitted by law; without even the
dnl implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
dnl
dnl Autodetects and sets language settings
AC_DEFUN([WUM_LANGUAGE],[
dnl
WUM_CHECK_CFLAGS([wum_CFLAGS_LANGUAGE],[
        [-std=c99]dnl
        [-pedantic-errors]dnl
])
AC_SUBST([wum_CFLAGS_LANGUAGE])
dnl
WUM_CHECK_LDFLAGS([wum_LDFLAGS_LANGUAGE],[[-Wl,--no-undefined]])
AC_SUBST([wum_LDFLAGS_LANGUAGE])
])
